﻿using RealEstateAgency.BLL.EntitiesDTO;
using RealEstateAgency.BLL.EntitiesDTO.EntityViewModelDTO;
using RealEstateAgency.BLL.Infrastuctures;
using RealEstateAgency.BLL.Interfaces;
using RealEstateAgency.BLL.Providers;
using RealEstateAgency.BLL.Specifications;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace RealEstateAgency.API.Controllers
{
    [RoutePrefix("Employee")]
    public class EmployeeController : ApiController
    {
        IEmployeeService employeeService;
        IEmployeeDismissService employeeDismissService;
        IEmployeePostService employeePostService;
        IEmployeeStatusService employeeStatusService;
        public EmployeeController(IEmployeeService employeeService, IEmployeeDismissService employeeDismissService,
                          IEmployeePostService employeePostService, IEmployeeStatusService employeeStatusService)
        {
            this.employeeService = employeeService;
            this.employeeDismissService = employeeDismissService;
            this.employeePostService = employeePostService;
            this.employeeStatusService = employeeStatusService;
        }

        #region Employee
        //-----Employee
        [Route("GetAllEmployees")]
        [HttpGet]
        public async Task<List<EmployeeDTO>> GetAllEmployees()
        {
            return await employeeService.GetAllEmployeesAsync();
        }
        [Route("GetAllEmployeesView")]
        [HttpGet]
        public async Task<List<EmployeeViewDTO>> GetAllEmployeesView()
        {
            var result = await employeeService.GetAllEmployeesViewAsync() ;
            return result;
        }
        [Route("GetEmployee")]
        [HttpGet]
        public async Task<EmployeeDTO> GetEmployee(string idEmployee)
        {
            return await employeeService.GetEmployeeByIdAsync(idEmployee);
        }
        [Route("GetEmployeeView")]
        [HttpGet]
        public async Task<EmployeeViewDTO> GetEmployeeView(string idEmployee)
        {
            return await employeeService.GetEmployeeViewByIdAsync(idEmployee);
        }
        [Route("CreateEmployee")]
        [HttpPost]
        public async Task<OperationDetails> CreateEmployee(EmployeeDTO EmployeeDto)
        {
            return await employeeService.CreateEmployeeAsync(EmployeeDto,
                new EmployeeMessageSpecification().ToSuccessCreateMessage(),
                new EmployeeMessageSpecification().ToFailCreateMessage());
        }
        [Route("CreateEmployeeView")]
        [HttpPost]
        public async Task<OperationDetails> CreateEmployeeView(EmployeeViewDTO EmployeeViewDto)
        {
            return await employeeService.CreateEmployeeViewAsync(EmployeeViewDto,
                new EmployeeMessageSpecification().ToSuccessCreateMessage(),
                new EmployeeMessageSpecification().ToFailCreateMessage());
        }
        [Route("CreateExistEmployee")]
        [HttpGet]
        public async Task<OperationDetails> CreateExistEmployee(string idEmployee)
        {
            return await employeeService.CreateExistEmployeeAsync(idEmployee,
                new EmployeeMessageSpecification().ToSuccessCreateExistMessage(),
                new EmployeeMessageSpecification().ToFailCreateExistMessage());
        }
        [Route("DismissEmployee")]
        [HttpGet]
        public async Task<OperationDetails> DismissEmployee(string idEmployee)
        {
            return await employeeService.DismissEmployeeAsync(idEmployee,
                new EmployeeMessageSpecification().ToSuccessDeleteMessage(),
                new EmployeeMessageSpecification().ToFailDeleteMessage());
        }
        [Route("UpdateEmployee")]
        [HttpPost]
        public async Task<OperationDetails> UpdateEmployee(EmployeeDTO EmployeeDto)
        {
            return await employeeService.UpdateEmployeeAsync(EmployeeDto,
                    new EmployeeMessageSpecification().ToSuccessUpdateMessage(),
                    new EmployeeMessageSpecification().ToFailUpdateMessage());
        }

        [Route("FilterEmployee")]
        [HttpPost]
        public async Task<List<EmployeeViewDTO>> FilterEmployee(EmployeeFilterModel EmployeeDto)
        {
            return await employeeService.FilterEmployeeAsync(EmployeeDto);        
        }
        #endregion

        #region EmployeeDismiss
        //-----EmployeePost
        [Route("GetAllDismiss")]
        [HttpGet]
        public async Task<List<EmployeeDismissDTO>> GetAllDismisses()
        {
            return await employeeDismissService.GetAllEmployeeDismissesAsync();
        }
        [Route("GetAllDismissesByEmployee")]
        [HttpGet]
        public async Task<List<EmployeeDismissDTO>> GetAllDismissesByEmployee(string employeeId)
        {
            string idEmployee = employeeId;
            return await employeeDismissService.GetAllEmployeeDismissesAsync(empl=>empl.EmployeeId== idEmployee);
        }
        [Route("GetDismiss")]
        [HttpGet]
        public async Task<EmployeeDismissDTO> GetDismiss(int idDismiss)
        {
            return await employeeDismissService.GetEmployeeDismissByIdAsync(idDismiss);
        }
        [Route("CreateDismiss")]
        [HttpPost]
        public async Task<OperationDetails> CreateDismiss(EmployeeDismissDTO DismissDto)
        {
            return await employeeDismissService.CreateEmployeeDismissAsync(DismissDto,
               new EmployeeDismissMessageSpecification().ToSuccessCreateMessage(),
               new EmployeeDismissMessageSpecification().ToFailCreateMessage());
        }
        [Route("DeleteDismiss")]
        [HttpGet]
        public async Task<OperationDetails> DeleteDismiss(int idDismiss)
        {
            return await employeeDismissService.DeleteEmployeeDismissAsync(idDismiss,
                new EmployeeDismissMessageSpecification().ToSuccessDeleteMessage(),
                new EmployeeDismissMessageSpecification().ToFailDeleteMessage());
        }
        [Route("UpdateDismiss")]
        [HttpPost]
        public async Task<OperationDetails> UpdateDismiss(EmployeeDismissDTO DismissDto)
        {
            return await employeeDismissService.UpdateEmployeeDismissAsync(DismissDto,
                    new EmployeeDismissMessageSpecification().ToSuccessUpdateMessage(),
                    new EmployeeDismissMessageSpecification().ToFailUpdateMessage());
        }

        [Route("FilterEmployeeDismiss")]
        [HttpPost]
        public async Task<List<EmployeeDismissDTO>> FilterEmployeeDismiss(EmployeeDismissFilterModel DismissDto)
        {
            return await employeeDismissService.FilterEmployeeDismissAsync(DismissDto);
        }
        #endregion

        #region EmployeePost
        //-----EmployeePost
        [Route("GetAllPosts")]
        [HttpGet]
        public async Task<List<EmployeePostDTO>> GetAllPosts()
        {
            return await employeePostService.GetAllEmployeePostsAsync();
        }
        [Route("GetPost")]
        [HttpGet]
        public async Task<EmployeePostDTO> GetPost(int idPost)
        {
            return await employeePostService.GetEmployeePostByIdAsync(idPost);
        }
        [Route("CreatePost")]
        [HttpPost]
        public async Task<OperationDetails> CreatePost(EmployeePostDTO PostDto)
        {
            return await employeePostService.CreateEmployeePostAsync(PostDto,
                new EmployeePostMessageSpecification(PostDto).ToSuccessCreateMessage(),
                new EmployeePostMessageSpecification(PostDto).ToFailCreateMessage());
        }
        [Route("DeletePost")]
        [HttpGet]
        public async Task<OperationDetails> DeletePost(int idPost)
        {
            return await employeePostService.DeleteEmployeePostAsync(idPost,
                new EmployeePostMessageSpecification().ToSuccessDeleteMessage(),
                new EmployeePostMessageSpecification().ToFailDeleteMessage());
        }
        [Route("UpdatePost")]
        [HttpPost]
        public async Task<OperationDetails> UpdatePost(EmployeePostDTO PostDto)
        {
            return await employeePostService.UpdateEmployeePostAsync(PostDto,
                    new EmployeePostMessageSpecification(PostDto).ToSuccessUpdateMessage(),
                    new EmployeePostMessageSpecification(PostDto).ToFailUpdateMessage());
        }
        [Route("FilterEmployeePost")]
        [HttpPost]
        public async Task<List<EmployeePostDTO>> FilterEmployeePost(EmployeePostFilterModel PostDto)
        {
            return await employeePostService.FilterEmployeePostAsync(PostDto);            
        }
        #endregion

        #region EmployeeStatus
        //-----EmployeeStatus
        [Route("GetAllStatuses")]
        [HttpGet]
        public async Task<List<EmployeeStatusDTO>> GetAllStatuses()
        {
            return await employeeStatusService.GetAllEmployeeStatusesAsync();
        }
        [Route("GetStatus")]
        [HttpGet]
        public async Task<EmployeeStatusDTO> GetStatus(int idStatus)
        {
            return await employeeStatusService.GetEmployeeStatusByIdAsync(idStatus);
        }
        [Route("CreateStatus")]
        [HttpPost]
        public async Task<OperationDetails> CreateStatus(EmployeeStatusDTO StatusDto)
        {
            return await employeeStatusService.CreateEmployeeStatusAsync(StatusDto,
                new EmployeeStatusMessageSpecification(StatusDto).ToSuccessCreateMessage(),
                new EmployeeStatusMessageSpecification(StatusDto).ToFailCreateMessage());
        }
        [Route("DeleteStatus")]
        [HttpGet]
        public async Task<OperationDetails> DeleteStatus(int idStatus)
        {
            return await employeeStatusService.DeleteEmployeeStatusAsync(idStatus,
                new EmployeeStatusMessageSpecification().ToSuccessDeleteMessage(),
                new EmployeeStatusMessageSpecification().ToFailDeleteMessage());
        }
        [Route("UpdateStatus")]
        [HttpPost]
        public async Task<OperationDetails> UpdateStatus(EmployeeStatusDTO StatusDto)
        {
            return await employeeStatusService.UpdateEmployeeStatusAsync(StatusDto,
                    new EmployeeStatusMessageSpecification(StatusDto).ToSuccessUpdateMessage(),
                    new EmployeeStatusMessageSpecification(StatusDto).ToFailUpdateMessage());
        }
        [Route("FilterEmployeeStatus")]
        [HttpPost]
        public async Task<List<EmployeeStatusDTO>> FilterEmployeeStatus(EmployeeStatusFilterModel StatusDto)
        {
            return await employeeStatusService.FilterEmployeeStatusAsync(StatusDto);
        }
        #endregion
    }
}
