﻿using RealEstateAgency.BLL.Entities.EntityViewModelDTO;
using RealEstateAgency.BLL.EntitiesDTO;
using RealEstateAgency.BLL.Infrastuctures;
using RealEstateAgency.BLL.Interfaces;
using RealEstateAgency.BLL.Specifications;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace RealEstateAgency.API.Controllers
{
    [RoutePrefix("Order")]
    public class OrderController : ApiController
    {
        IOrderService orderService;
        public OrderController(IOrderService orderService)
        {
            this.orderService = orderService;
        }

        #region Order
        //-----Order
        //[Route("GetAllOrdersView")]
        //[HttpGet]
        //public async Task<List<OrderViewDTO>> GetAllOrdersView(string employeeId)
        //{
        //    var listOrders = await orderService.GetAllOrdersAsync(x => x.EmployeeID == employeeId);
        //    return await orderService.GetAllOrdersViewAsync(listOrders);
        //}
        [Route("GetOrdersViewByPersonId")]
        [HttpPost]
        public async Task<List<OrderViewModel>> GetOrdersViewByPersonId(ResponseSignInModel personOrderModel)
        {
            return await orderService.GetOrdersViewByPersonIdAsync(personOrderModel);
        }
        [Route("GetOrder")]
        [HttpGet]
        public async Task<OrderDTO> GetOrder(int idOrder)
        {
            return await orderService.GetOrderByIdAsync(idOrder);
        }
        [Route("CreateOrder")]
        [HttpPost]
        public async Task<OperationDetails> CreateOrder(OrderDTO OrderDto)
        {
            return await orderService.CreateOrderAsync(OrderDto,
               new OrderMessageSpecification().ToSuccessCreateMessage(),
               new OrderMessageSpecification().ToFailCreateMessage());
        }
        [Route("DeleteOrder")]
        [HttpGet]
        public async Task<OperationDetails> DeleteOrder(int idOrder)
        {
            return await orderService.DeleteOrderAsync(idOrder,
                new OrderMessageSpecification().ToSuccessDeleteMessage(),
                new OrderMessageSpecification().ToFailDeleteMessage());
        }
        [Route("UpdateOrder")]
        [HttpPost]
        public async Task<OperationDetails> UpdateOrder(OrderDTO OrderDto)
        {
            return await orderService.UpdateOrderAsync(OrderDto,
                    new OrderMessageSpecification().ToSuccessUpdateMessage(),
                    new OrderMessageSpecification().ToFailUpdateMessage());
        }

        [Route("GetOrderBusyTimeByDate")]
        [HttpGet]
        public async Task<List<int>> GetOrderBusyTimeByDate(string date,int realEstateId)
        {
            return await orderService.GetOrderBusyTimeByDate(date, realEstateId);
        }
        #endregion
    }
}
