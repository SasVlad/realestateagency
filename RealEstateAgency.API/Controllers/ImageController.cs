﻿using RealEstateAgency.BLL.EntitiesDTO;
using RealEstateAgency.BLL.Infrastuctures;
using RealEstateAgency.BLL.Providers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace RealEstateAgency.API.Controllers
{
    [RoutePrefix("Image")]
    public class ImageController : ApiController
    {
        [Route("UploadImage")]
        [HttpPost]
        public async Task<OperationDetails> UploadImage(UploadImage image)
        {
            var imageName = new ImageProvider().UploadImage(image.ImageBase64, image.ImageName);
            return new OperationDetails(true, imageName, ""); 
        }
    }
}
