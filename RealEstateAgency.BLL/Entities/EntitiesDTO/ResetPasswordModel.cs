﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RealEstateAgency.BLL.Entities.EntitiesDTO
{
    public class ResetPasswordModel
    {
        public string UserID { get; set; }
        public string NewPassword { get; set; }
        public string RepeateNewPassword { get; set; }
    }
}
