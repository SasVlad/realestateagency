﻿using FluentValidation;
using FluentValidation.Attributes;
using RealEstateAgency.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RealEstateAgency.BLL.EntitiesDTO
{
    [Validator(typeof(OrderDTOValidator))]
    public class OrderDTO
    {
        public int? Id { get; set; }

        public string UserId { get; set; }

        public string EmployeeId { get; set; }

        public int RealEstateId { get; set; }

        public string DateViewRealEstate { get; set; }

        public int TimeDateViewRealEstate { get; set; }

        public string DateRecord { get; set; }

        public OrderState OrderState { get; set; }
    }
    public class OrderDTOValidator : AbstractValidator<OrderDTO>
    {
        public OrderDTOValidator()
        {
            RuleFor(ac => ac.UserId)
                .NotEmpty().WithMessage("The User of Order cannot be blank.");
            RuleFor(ac => ac.EmployeeId)
                .NotEmpty().WithMessage("The RealEstate of Order cannot be blank.");
            RuleFor(ac => ac.RealEstateId)
                .NotEmpty().WithMessage("The Price cannot be blank.");
            RuleFor(ac => ac.DateViewRealEstate)
                .NotEmpty().WithMessage("The Date View RealEstate of Order cannot be blank.");
            RuleFor(ac => ac.TimeDateViewRealEstate)
            .NotEmpty().WithMessage("The Time View RealEstate of Order cannot be blank.");
            RuleFor(ac => ac.DateRecord)
                .NotEmpty().WithMessage("The Date Record of Order cannot be blank.");
        }
    }
}
