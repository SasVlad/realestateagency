﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RealEstateAgency.BLL.EntitiesDTO
{
    public class ImageDTO
    {
        public Guid Id { get; set; }
        public Guid ItemId { get; set; }
        public string ImageUrl { get; set; }
    }
}
