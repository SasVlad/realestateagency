﻿using RealEstateAgency.BLL.EntitiesDTO;
using RealEstateAgency.BLL.EntitiesDTO.EntityViewModelDTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RealEstateAgency.BLL.Entities.EntityViewModelDTO
{
    public class OrderViewModel
    {
        public OrderViewModel()
        {
            Order = new OrderDTO();
            UserView = new UserViewDTO();
            RealEstateView = new RealEstateViewDTO();
            EmployeeView = new EmployeeViewDTO();
        }

        public OrderDTO Order { get; set; }
        public UserViewDTO UserView { get; set; }
        public RealEstateViewDTO RealEstateView { get; set; }
        public EmployeeViewDTO EmployeeView { get; set; }
    }
}
