﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RealEstateAgency.BLL.Entities.EntityViewModelDTO
{
    public class ResponseSignInModel
    {
        public string Id { get; set; }
        public string Role { get; set; }
    }
}
