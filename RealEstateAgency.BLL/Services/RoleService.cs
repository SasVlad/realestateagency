﻿using Microsoft.AspNet.Identity.EntityFramework;
using RealEstateAgency.BLL.Interfaces;
using RealEstateAgency.DAL.Entities;
using RealEstateAgency.DAL.Identity;
using RealEstateAgency.DAL.Interfaces;
using RealEstateAgency.DAL.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RealEstateAgency.BLL.Services
{
    public class RoleService : IRoleService
    {
        ApplicationRoleManager _roleManager;
        IRepository<IdentityUserRole, string> _roleRepository;

        public RoleService(
                            IdentityDbContext identityDbContext,
                            IRepository<IdentityUserRole, string> roleRepository)
        {
            _roleManager = new UnitOfWorkIdentity(identityDbContext).RoleManager;
            this._roleRepository = roleRepository;
        }
        public async Task<string> GetRoleByPersonId(string personId)
        {
            var role = await _roleRepository.FindOneAsync(x => x.UserId == personId);
            var roleId = role.RoleId;
            var roleResult = await _roleManager.FindByIdAsync(roleId);
            return roleResult.Name;
        }
    }
}
