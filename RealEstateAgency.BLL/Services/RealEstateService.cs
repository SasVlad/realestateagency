﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RealEstateAgency.DAL.Interfaces;
using RealEstateAgency.DAL.Entities;
using RealEstateAgency.DAL.Repositories;
using RealEstateAgency.DAL.EF;
using RealEstateAgency.BLL.Infrastuctures;
using RealEstateAgency.BLL.EntitiesDTO;
using RealEstateAgency.BLL.Interfaces;
using RealEstateAgency.BLL.Service;
using System.Linq.Expressions;
using RealEstateAgency.BLL.Specifications;
using RealEstateAgency.BLL.EntitiesDTO.EntityViewModelDTO;
using Ninject;
using System.Diagnostics;

namespace RealEstateAgency.BLL.Services
{
    public class RealEstateService : IRealEstateService
    {
        IRepository<RealEstate, int> _realEstateRepository;
        IServiceT<RealEstate, RealEstateDTO, int> _realEstateService;
        IAddressService _addressService;
        IRealEstateClassService _classService;
        IRealEstateStatusService _statusService;
        IRealEstateTypeService _typeService;
        IRealEstateTypeWallService _typeWallRealEstate;
        IEmployeeService _employeeService;
        public RealEstateService(IRepository<RealEstate, int> repository,
                                 IServiceT<RealEstate, RealEstateDTO, int> service,
                                 IAddressService addressService,
                                 IRealEstateClassService classService,
                                 IRealEstateStatusService statusService,
                                 IRealEstateTypeService typeService,
                                 IRealEstateTypeWallService typeWallRealEstate,
                                 IEmployeeService employeeService)
        {
            this._realEstateRepository = repository;
            this._realEstateService = service;
            this._addressService = addressService;
            this._classService = classService;
            this._statusService = statusService;
            this._typeService = typeService;
            this._typeWallRealEstate = typeWallRealEstate;
            this._employeeService = employeeService;
        }
        public async Task<List<RealEstateViewDTO>> GetRealEstateViewList(
            List<RealEstateDTO> realestateList,List<AddressViewDTO> addressList)
        {
            List<RealEstateViewDTO> listRealEstateView = new List<RealEstateViewDTO>();
            List<RealEstateClassDTO> listRealEstateClass = await _classService.GetAllRealEstateClassesAsync();
            List<RealEstateStatusDTO> listRealEstateStatus = await _statusService.GetAllRealEstateStatusesAsync() ;
            List<RealEstateTypeDTO> listRealEstateType= await _typeService.GetAllRealEstateTypesAsync();
            List<RealEstateTypeWallDTO> listRealEstateTypeWall = await _typeWallRealEstate.GetAllRealEstateTypeWallsAsync();
            List<EmployeeDTO> listEmployees = await this._employeeService.GetAllEmployeesAsync();
            Parallel.ForEach(realestateList, item =>
             {
                 if (addressList.Where(a => a.Address.AddressID == item.AddressID).AsParallel().Count() != 0)
                 {
                     listRealEstateView.Add(
                                      new RealEstateViewDTO
                                      {
                                          RealEstate = item,
                                          RealEstateClass = listRealEstateClass.Find(x=>x.RealEstateClassID==item.RealEstateClassID),
                                          RealEstateStatus = listRealEstateStatus.Find(x => x.RealEstateStatusID == item.RealEstateStatusID),
                                          RealEstateType = listRealEstateType.Find(x => x.RealEstateTypeID == item.RealEstateTypeID),
                                          RealEstateTypeWall = listRealEstateTypeWall.Find(x => x.RealEstateTypeWallID == item.RealEstateTypeWallID),
                                          AddressView = addressList.Find(a => a.Address.AddressID == item.AddressID),
                                          Employee = listEmployees.Find(e => e.PersonId == item.EmployeeId)
                                      }
                                 );
                 }
             });
            return listRealEstateView;
        }
        public async Task<RealEstateViewDTO> GetRealEstateViewByIdAsync(int id)
        {
            var realestate = await _realEstateService.GetItemByIdAsync(id);

            RealEstateViewDTO realEstateView = new RealEstateViewDTO();
            realEstateView.RealEstate = realestate;
            realEstateView.RealEstateClass = await _classService.GetRealEstateClassByIdAsync(realestate.RealEstateClassID);
            realEstateView.RealEstateStatus = await _statusService.GetRealEstateStatusByIdAsync(realestate.RealEstateStatusID);
            realEstateView.RealEstateType = await _typeService.GetRealEstateTypeByIdAsync(realestate.RealEstateTypeID);
            realEstateView.RealEstateTypeWall = await _typeWallRealEstate.GetRealEstateTypeWallByIdAsync(realestate.RealEstateTypeWallID);
            realEstateView.AddressView = await _addressService.GetAddressViewByIdAsync(realestate.AddressID);
            realEstateView.Employee = await _employeeService.GetEmployeeByIdAsync(realestate.EmployeeId);
            return realEstateView;
        }
        public async Task<List<RealEstateDTO>> GetAllRealEstatesAsync(Expression<Func<RealEstateDTO, bool>> where = null)
        {
            return await _realEstateService.GetAllItemsAsync(where);
        }
        public async Task<List<RealEstateViewDTO>> GetAllRealEstatesViewAsync(Expression<Func<RealEstateDTO, bool>> where = null)
        {        
            List<RealEstateDTO> listRealEstates = new List<RealEstateDTO>();
            List<AddressViewDTO> listAddresses = new List<AddressViewDTO>();
            listRealEstates = await _realEstateService.GetAllItemsAsync(where);
            listAddresses = await _addressService.GetAllAddressesViewAsync();
            return await this.GetRealEstateViewList(listRealEstates, listAddresses);
            
        }

        public async Task<RealEstateDTO> GetRealEstateByIdAsync(int id)
        {
            return await _realEstateService.GetItemByIdAsync(id);
        }


        public async Task<RealEstateDTO> GetRealEstateByParamsAsync(Expression<Func<RealEstateDTO, bool>> where)
        {
            return await _realEstateService.GetItemByParamsAsync(where);
        }


        public async Task<OperationDetails> CreateRealEstateAsync(RealEstateViewDTO realEstateViewDto, OperationDetails MessageSuccess, OperationDetails MessageFail)
        {
            var resultAddress = await _addressService.CreateAddressAsync
                         (
                            realEstateViewDto.AddressView.Address,
                            new AddressMessageSpecification().ToSuccessCreateMessage(),
                            new AddressMessageSpecification().ToFailCreateMessage()
                         );
            string addressId = resultAddress.Id;

            realEstateViewDto.RealEstate.AddressID = Convert.ToInt32(addressId);
            var resultRealEstate=await _realEstateService.CreateItemAsync
                        (
                           realEstateViewDto.RealEstate,
                           new RealEstateEquelSpecification(realEstateViewDto.RealEstate).ToExpression(),
                           MessageSuccess,
                           MessageFail
                        );
            return new OperationDetails
                        (
                         resultRealEstate.Item1.Successed,
                         resultRealEstate.Item1.Message,
                         resultRealEstate.Item1.Property,
                         resultRealEstate.Item2.RealEstateID.ToString());
        }

        public async Task<OperationDetails> DeleteRealEstateAsync(int id, OperationDetails MessageSuccess, OperationDetails MessageFail)
        {
            return await _realEstateService.DeleteItemAsync(id,
                MessageSuccess,
                MessageFail);
        }

        public async Task<OperationDetails> UpdateRealEstateAsync(RealEstateDTO realEstateDto, OperationDetails MessageSuccess, OperationDetails MessageFail)
        {
            int idRealEstate = realEstateDto.RealEstateID;
            return await _realEstateService.UpdateItemAsync(realEstateDto,
                idRealEstate,
                MessageSuccess,
                MessageFail);
        }
        public void Dispose()
        {
            _realEstateRepository.Dispose();
        }

        public async Task<List<RealEstateDTO>> FilterRealEstateAsync(RealEstateFilterModel realEstateFilter)
        {
            List<RealEstateDTO> list = await this.GetAllRealEstatesAsync();
            if (realEstateFilter.RealEstateID != null) list = list.Where(emp => emp.RealEstateID == realEstateFilter.RealEstateID).ToList();
            if (realEstateFilter.RealEstateClassID != null) list = list.Where(emp => emp.RealEstateClassID == realEstateFilter.RealEstateClassID).ToList();
            if (realEstateFilter.RealEstateStatusID != null) list = list.Where(emp => emp.RealEstateStatusID == realEstateFilter.RealEstateStatusID).ToList();
            if (realEstateFilter.RealEstateTypeID != null) list = list.Where(emp => emp.RealEstateTypeID == realEstateFilter.RealEstateTypeID).ToList();
            if (realEstateFilter.RealEstateTypeWallID != null) list = list.Where(emp => emp.RealEstateTypeWallID == realEstateFilter.RealEstateTypeWallID).ToList();
            if (realEstateFilter.AddressID != null) list = list.Where(emp => emp.AddressID == realEstateFilter.AddressID).ToList();
            if (realEstateFilter.Elevator != null) list = list.Where(emp => emp.Elevator == realEstateFilter.Elevator).ToList();
            if (realEstateFilter.NearSubway != null) list = list.Where(emp => emp.NearSubway == realEstateFilter.NearSubway).ToList();
            if (realEstateFilter.BeginLevel != null) list = list.Where(emp => emp.NumberOfRooms >= realEstateFilter.BeginLevel).ToList();
            if (realEstateFilter.EndLevel != null) list = list.Where(emp => emp.Price <= realEstateFilter.EndLevel).ToList();
            if (realEstateFilter.BeginPrice != null) list = list.Where(emp => emp.Price >= realEstateFilter.BeginPrice).ToList();
            if (realEstateFilter.EndPrice != null) list = list.Where(emp => emp.Price <= realEstateFilter.EndPrice).ToList();
            if (realEstateFilter.BeginGrossArea != null) list = list.Where(emp => emp.GrossArea >= realEstateFilter.BeginGrossArea).ToList();
            if (realEstateFilter.EndGrossArea != null) list = list.Where(emp => emp.GrossArea <= realEstateFilter.EndGrossArea).ToList();
            if (realEstateFilter.BeginNumberOfRooms != null) list = list.Where(emp => emp.NumberOfRooms >= realEstateFilter.BeginNumberOfRooms).ToList();
            if (realEstateFilter.EndNumberOfRooms != null) list = list.Where(emp => emp.NumberOfRooms <= realEstateFilter.EndNumberOfRooms).ToList();
            return list;
        }
        public async Task<List<RealEstateViewDTO>> GetAllFilterRealEstatesViewAsync(RealEstateFilterViewDTO realEstateFilterView)
        {
            var listRealEstates = await FilterRealEstateAsync(realEstateFilterView.RealEstateFilter);
            var listAddresses = await _addressService.FilterAddressAsync(realEstateFilterView.AddressFilter);
            var listAddressesViewDto = await _addressService.GetAllAddressesViewAsync(listAddresses);
            return await this.GetRealEstateViewList(listRealEstates, listAddressesViewDto);
        }
    }
}
