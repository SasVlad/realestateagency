﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RealEstateAgency.DAL.Interfaces;
using RealEstateAgency.DAL.Entities;
using RealEstateAgency.DAL.Repositories;
using RealEstateAgency.DAL.EF;
using RealEstateAgency.BLL.Infrastuctures;
using RealEstateAgency.BLL.EntitiesDTO;
using RealEstateAgency.BLL.Interfaces;
using RealEstateAgency.BLL.Service;
using System.Linq.Expressions;
using RealEstateAgency.BLL.Specifications;
using RealEstateAgency.BLL.EntitiesDTO.EntityViewModelDTO;
using RealEstateAgency.BLL.Entities.EntityViewModelDTO;

namespace RealEstateAgency.BLL.Services
{
    public class OrderService : IOrderService
    {
        IRepository<Order, int> repository;
        IServiceT<Order, OrderDTO, int> service;
        IUserService UserService;
        IEmployeeService EmployeeService;
        IRealEstateService RealEstateService;
        IContractTypeService ContractTypeService;
        public OrderService(IRepository<Order, int> repository,
                                         IServiceT<Order, OrderDTO, int> service,
                                         IUserService userService,
                                         IEmployeeService employeeService,
                                         IRealEstateService realEstateService,
                                         IContractTypeService contractTypeService
                                         )
        {
            this.ContractTypeService = contractTypeService;
            this.repository = repository;
            this.service = service;
            this.UserService = userService;
            this.EmployeeService = employeeService;
            this.RealEstateService = realEstateService;
        }

        public async Task<OperationDetails> CreateOrderAsync(OrderDTO orderDto, OperationDetails MessageSuccess, OperationDetails MessageFail)
        {
            return (await service.CreateItemAsync(orderDto,
               new OrderEquelSpecification(orderDto).ToExpression(),
                MessageSuccess,
                MessageFail)).Item1;
        }

        public Task<OperationDetails> DeleteOrderAsync(int id, OperationDetails MessageSuccess, OperationDetails MessageFail)
        {
            throw new NotImplementedException();
        }

        public void Dispose()
        {
            throw new NotImplementedException();
        }

        public Task<List<OrderDTO>> GetAllOrdersAsync(Expression<Func<OrderDTO, bool>> where = null)
        {
            throw new NotImplementedException();
        }

        public async Task<List<OrderViewModel>> GetOrdersViewByPersonIdAsync(ResponseSignInModel personOrderModel)
        {
            List<OrderViewModel> listOrdersView = new List<OrderViewModel>();

            List<OrderDTO> listOrders = null;
            if (personOrderModel.Role=="User")
            {
                listOrders= await this.service.GetAllItemsAsync(x => x.UserId == personOrderModel.Id);
            }
            if (personOrderModel.Role != "User")
            {
                listOrders = await this.service.GetAllItemsAsync(x => x.EmployeeId == personOrderModel.Id);
            }
            List<UserViewDTO> listUsersView = await this.UserService.GetAllUsersViewAsync();
            List<EmployeeViewDTO> listEmployees = await this.EmployeeService.GetAllEmployeesViewAsync();
            List<RealEstateViewDTO> listRealEstatesView = await this.RealEstateService.GetAllRealEstatesViewAsync();

            Parallel.ForEach(listOrders, item =>
            {

                listOrdersView.Add(
                                     new OrderViewModel
                                     {
                                         Order = item,
                                         UserView = listUsersView.Find(x => x.Person.PersonId == item.UserId),
                                         EmployeeView = listEmployees.Find(e => e.Person.PersonId == item.EmployeeId),
                                         RealEstateView = listRealEstatesView.Find(x => x.RealEstate.RealEstateID == item.RealEstateId),
                                     }
                                );
                
            });
            return listOrdersView;
        }

        public Task<OrderDTO> GetOrderByIdAsync(int id)
        {
            throw new NotImplementedException();
        }

        public Task<OrderDTO> GetOrderByParamsAsync(Expression<Func<OrderDTO, bool>> where)
        {
            throw new NotImplementedException();
        }

        public async Task<OperationDetails> UpdateOrderAsync(OrderDTO orderDto, OperationDetails MessageSuccess, OperationDetails MessageFail)
        {
            int idOrderDto = Convert.ToInt32(orderDto.Id);
            return await service.UpdateItemAsync(orderDto,
                idOrderDto,
                MessageSuccess,
                MessageFail);
        }

        public async Task<List<int>> GetOrderBusyTimeByDate(string date,int realEstateId)
        {
            var listOrders = await this.service.GetAllItemsAsync(x => x.DateViewRealEstate == date && x.RealEstateId==realEstateId && x.OrderState!=OrderState.Cancelled);
            return listOrders.Select(x => x.TimeDateViewRealEstate).ToList();
        }


        //public async Task<List<ContractViewDTO>> GetAllContractsViewAsync()
        //{
        //    List<ContractViewDTO> listContractsView = new List<ContractViewDTO>();

        //    List<ContractDTO> listContracts = await this.service.GetAllItemsAsync();
        //    List<ContractTypeDTO> listContractTypes = await this.ContractTypeService.GetAllContractTypesAsync();
        //    List<UserViewDTO> listUsersView = await this.UserService.GetAllUsersViewAsync();
        //    List<EmployeeDTO> listEmployees = await this.EmployeeService.GetAllEmployeesAsync();
        //    List<RealEstateViewDTO> listRealEstatesView = await this.RealEstateService.GetAllRealEstatesViewAsync();

        //    List<ContractViewDTO> AllList = listContracts
        //        .Join(
        //            listUsersView,
        //            c => c.SellerID,
        //            u => u.Person.PersonId,
        //            (c, u) => new ContractViewDTO
        //            {
        //                ContractType = listContractTypes.Find(x=>x.ContractTypeID==c.ContractTypeID),
        //                UserView = u,
        //                Contract = c,
        //                Employee = listEmployees.Find(e => e.PersonId == c.EmployeeID),
        //                RealEstateView = listRealEstatesView.Find(r=>r.RealEstate.RealEstateID==c.RealEstateID)                      

        //            }).ToList();

        //    return AllList;
        //}

        //public async Task<List<ContractDTO>> GetAllContractsAsync(Expression<Func<ContractDTO, bool>> where = null)
        //{
        //    return await service.GetAllItemsAsync(where);
        //}

        //public async Task<ContractDTO> GetContractByIdAsync(int id)
        //{
        //    return await service.GetItemByIdAsync(id);
        //}

        //public async Task<ContractDTO> GetContractByParamsAsync(Expression<Func<ContractDTO, bool>> where)
        //{
        //    return await service.GetItemByParamsAsync(where);
        //}
        //public async Task<OperationDetails> CreateContractAsync(ContractDTO contractDto, OperationDetails MessageSuccess, OperationDetails MessageFail)
        //{
        //    
        //}

        //public async Task<OperationDetails> DeleteContractAsync(int id, OperationDetails MessageSuccess, OperationDetails MessageFail)
        //{
        //    return await service.DeleteItemAsync(id,
        //        MessageSuccess,
        //        MessageFail);
        //}

        //public async Task<OperationDetails> UpdateContractAsync(ContractDTO contractDto, OperationDetails MessageSuccess, OperationDetails MessageFail)
        //{
        //    int idContractDto = contractDto.ContractID;
        //    return await service.UpdateItemAsync(contractDto,
        //        idContractDto,
        //        MessageSuccess,
        //        MessageFail);
        //}
        //public void Dispose()
        //{
        //    repository.Dispose();
        //}

        //public async Task<List<ContractDTO>> FilterContractAsync(ContractFilterModel contractFilter)
        //{
        //    List<ContractDTO> list = await this.GetAllContractsAsync();
        //    if (contractFilter.BuyerID != null) list = list.Where(emp => emp.BuyerID == contractFilter.BuyerID).ToList();
        //    if (contractFilter.ContractID != null) list = list.Where(emp => emp.ContractID == contractFilter.ContractID).ToList();
        //    if (contractFilter.ContractTypeID != null) list = list.Where(emp => emp.ContractTypeID == contractFilter.ContractTypeID).ToList();
        //    if (contractFilter.EmployeeID != null) list = list.Where(emp => emp.EmployeeID == contractFilter.EmployeeID).ToList();
        //    if (contractFilter.RealEstateID != null) list = list.Where(emp => emp.RealEstateID == contractFilter.RealEstateID).ToList();
        //    if (contractFilter.SellerID != null) list = list.Where(emp => emp.SellerID == contractFilter.SellerID).ToList();
        //    if (contractFilter.RecordDate != null) list = list.Where(emp => emp.RecordDate == contractFilter.RecordDate).ToList();
        //    //if (contractFilter.BeginRecordDate != null) list = list.Where(emp => emp.BeginRecordDate == contractFilter.BeginRecordDate).ToList();
        //    //if (contractFilter.EndRecordDate != null) list = list.Where(emp => emp.EndRecordDate == contractFilter.EndRecordDate).ToList();
        //    return list;
        //}
    }
}
