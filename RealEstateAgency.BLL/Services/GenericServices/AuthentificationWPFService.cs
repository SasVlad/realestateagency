﻿using Microsoft.AspNet.Identity;
using Ninject;
using RealEstateAgency.BLL.Entities.EntitiesDTO;
using RealEstateAgency.BLL.Entities.EntityViewModelDTO;
using RealEstateAgency.BLL.EntitiesDTO;
using RealEstateAgency.BLL.Interfaces;
using RealEstateAgency.BLL.Interfaces.GenericInterfaces;
using RealEstateAgency.DAL.Entities;
using RealEstateAgency.DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace RealEstateAgency.BLL.Services.GenericServices
{
    public class AuthentificationWPFService : IAuthentificationService<string>
    {
        IKernel kernel;
        IUnitOfWorkIdentity identity;
        IRoleService _roleService;
        public AuthentificationWPFService(IKernel kernel,IRoleService roleService)
        {
            this.kernel = kernel;
            this.identity = kernel.Get<IUnitOfWorkIdentity>();
            this._roleService = roleService;
        }
        public async Task<string> AuthenticateAsync(PersonAbstractDTO PersonDto)
        {
            string Auth = "";
            ApplicationUser user = await identity.AppUserManager.FindAsync(PersonDto.Email, PersonDto.Password);
            
            if (user != null)
            {
                Auth = user.Id;
            }
            return Auth;
        }
        public async Task<ResponseSignInModel> AuthenticateWithRoleAsync(PersonAbstractDTO PersonDto)
        {
            ResponseSignInModel auth = null; 
            ApplicationUser user = await identity.AppUserManager.FindAsync(PersonDto.Email, PersonDto.Password);
            if (user != null)
            {
                var roleName = await _roleService.GetRoleByPersonId(user.Id);
                auth = new ResponseSignInModel();
                auth.Id = user.Id;
                auth.Role = roleName;
            }
            return auth;
        }
        public async Task SetInitialDataAsync(PersonAbstractDTO PersonDto, List<string> roles)
        {
           await new AuthentificationHttpService(kernel).SetInitialDataAsync(PersonDto,roles);
        }
        public async void ResetPassword(ResetPasswordModel resetPasswordModel)
        {
            identity.AppUserManager.RemovePassword(resetPasswordModel.UserID);

            identity.AppUserManager.AddPassword(resetPasswordModel.UserID, resetPasswordModel.NewPassword);
            //string resetToken = await identity.AppUserManager.GeneratePasswordResetTokenAsync(resetPasswordModel.UserID);
            //IdentityResult passwordChangeResult = await identity.AppUserManager.ResetPasswordAsync(resetPasswordModel.UserID, resetToken, resetPasswordModel.NewPassword);
        }
    }
}
