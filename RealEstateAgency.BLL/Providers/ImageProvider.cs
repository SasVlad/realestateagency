﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RealEstateAgency.BLL.Providers
{
    public class ImageProvider
    {
        public string UploadImage(string ImageBase64,string imageName)
        {
            var guid = Guid.NewGuid();
            string imageNameWithExtension = $"{imageName}_{guid}.png";
            try
            {
                //--------need refactor
                string x = ImageBase64.Replace("data:image/png;base64,", "").Replace("data:image/jpeg;base64,", "");
                byte[] imageBytes = Convert.FromBase64String(x);
                MemoryStream ms = new MemoryStream(imageBytes, 0, imageBytes.Length);

                ms.Write(imageBytes, 0, imageBytes.Length);
                var imagePath = $"{AppDomain.CurrentDomain.BaseDirectory}images\\{imageNameWithExtension}";
                Image image = Image.FromStream(ms, true);
                image.Save(imagePath, ImageFormat.Png);
                return imageNameWithExtension;
            }
            catch (Exception ex)
            {
                return "";
            }
            
        }
    }
}
