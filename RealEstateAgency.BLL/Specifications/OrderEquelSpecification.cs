﻿using RealEstateAgency.BLL.EntitiesDTO;
using RealEstateAgency.BLL.Infrastuctures;
using RealEstateAgency.BLL.Interfaces;
using RealEstateAgency.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace RealEstateAgency.BLL.Specifications
{
    public class OrderEquelSpecification : Specification<Order, OrderDTO>
    {
        OrderDTO _orderDto;
        public OrderEquelSpecification(OrderDTO orderDto)
        {
            this._orderDto = orderDto;
        }
        public override Expression<Func<Order, bool>> ToExpression()
        {
            return or => or.Id == _orderDto.Id;// or => or.RealEstateId == _orderDto.RealEstateId && or.UserId == _orderDto.UserId;
        }
    }
    public class OrderMessageSpecification : SpecificationMessage
    {
        OrderDTO OrderDto;
        public OrderMessageSpecification(OrderDTO contractDto = null)
        {
            OrderDto = contractDto;
        }

        public override OperationDetails ToSuccessCreateMessage()
        {
            return new OperationDetails(true, $"Заявка успешно добавлен", "");
        }

        public override OperationDetails ToSuccessDeleteMessage()
        {
            return new OperationDetails(true, $"Заявка успешно удален", "");
        }

        public override OperationDetails ToSuccessUpdateMessage()
        {
            return new OperationDetails(true, $"Заявка успешно изменена", "");
        }

        public override OperationDetails ToFailCreateMessage()
        {
            return new OperationDetails(false, $"Заявка уже существует", "Order");
        }

        public override OperationDetails ToFailDeleteMessage()
        {
            return new OperationDetails(false, "Такой заявки нет в базе данных", "Order");
        }

        public override OperationDetails ToFailUpdateMessage()
        {
            return new OperationDetails(false, $"Такой заявка нет в базе данных", "Order");
        }


    }
}
