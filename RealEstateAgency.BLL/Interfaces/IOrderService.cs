﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RealEstateAgency.BLL.EntitiesDTO;
using RealEstateAgency.BLL.Infrastuctures;
using System.Linq.Expressions;
using RealEstateAgency.DAL.Entities;
using RealEstateAgency.BLL.EntitiesDTO.EntityViewModelDTO;
using RealEstateAgency.BLL.Entities.EntityViewModelDTO;

namespace RealEstateAgency.BLL.Interfaces
{
    public interface IOrderService : IDisposable
    {
        Task<List<OrderViewModel>> GetOrdersViewByPersonIdAsync(ResponseSignInModel personOrderModel);
        Task<List<OrderDTO>> GetAllOrdersAsync(Expression<Func<OrderDTO, bool>> where = null);
        Task<OrderDTO> GetOrderByIdAsync(int id);
        Task<OrderDTO> GetOrderByParamsAsync(Expression<Func<OrderDTO, bool>> where);
        Task<OperationDetails> CreateOrderAsync(OrderDTO orderDto, OperationDetails MessageSuccess, OperationDetails MessageFail);
        Task<OperationDetails> DeleteOrderAsync(int id, OperationDetails MessageSuccess, OperationDetails MessageFail);
        Task<OperationDetails> UpdateOrderAsync(OrderDTO orderDto, OperationDetails MessageSuccess, OperationDetails MessageFail);
        Task<List<int>> GetOrderBusyTimeByDate(string date,int realEstateId);
        //Task<List<OrderDTO>> FilterOrderAsync(OrderFilterModel orderFilter);
    }
}
