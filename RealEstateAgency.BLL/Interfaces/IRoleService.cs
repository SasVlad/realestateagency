﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RealEstateAgency.BLL.Interfaces
{
    public interface IRoleService
    {
        Task<string> GetRoleByPersonId(string personId);
    }
}
