﻿using RealEstateAgency.BLL.EntitiesDTO;
using RealEstateAgency.BLL.Infrastuctures;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace RealEstateAgency.BLL.Interfaces
{
    public interface IImageService : IDisposable
    {
        Task<List<ImageDTO>> GetAllImagesAsync(Expression<Func<ImageDTO, bool>> where = null);
        Task<ImageDTO> GetImageByIdAsync(Guid id);
        Task<ImageDTO> GetImageByParamsAsync(Expression<Func<ImageDTO, bool>> where);
        Task<OperationDetails> CreateImageAsync(ImageDTO orderDto, OperationDetails MessageSuccess, OperationDetails MessageFail);
        Task<OperationDetails> DeleteImageAsync(Guid id, OperationDetails MessageSuccess, OperationDetails MessageFail);
        Task<OperationDetails> UpdateImageAsync(ImageDTO orderDto, OperationDetails MessageSuccess, OperationDetails MessageFail);
        //Task<List<OrderDTO>> FilterOrderAsync(OrderFilterModel orderFilter);
    }
}
