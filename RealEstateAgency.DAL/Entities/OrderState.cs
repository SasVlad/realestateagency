﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RealEstateAgency.DAL.Entities
{
    public enum OrderState
    {
        Confirmed,
        NotConfirmed,
        Cancelled,
        InReview
    }
}
