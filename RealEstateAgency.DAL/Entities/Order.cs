﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RealEstateAgency.DAL.Entities
{
    public class Order
    {
        public int Id { get; set; }

        public string UserId { get; set; }

        public string EmployeeId { get; set; }

        public int RealEstateId { get; set; }

        public virtual User User { get; set; }

        public virtual Employee Employee { get; set; }

        public virtual RealEstate RealEstate { get; set; }

        public string DateViewRealEstate { get; set; }

        public int TimeDateViewRealEstate { get; set; }

        public string DateRecord { get; set; }

        public OrderState OrderState { get; set; }
    }
}
