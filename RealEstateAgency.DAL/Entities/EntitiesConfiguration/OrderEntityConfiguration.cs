﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity.ModelConfiguration;

namespace RealEstateAgency.DAL.Entities.EntitiesConfiguration
{
    public class OrderEntityConfiguration : EntityTypeConfiguration<Order>
    {
        public OrderEntityConfiguration(){
            this.HasKey<int>(e => e.Id);
            this.HasRequired(o => o.Employee)
            .WithMany()
            .HasForeignKey(a => a.EmployeeId)
            .WillCascadeOnDelete(false);

            this.HasRequired(o => o.RealEstate)
            .WithMany()
            .HasForeignKey(a => a.RealEstateId)
            .WillCascadeOnDelete(false);

            this.HasRequired(o => o.User)
            .WithMany()
            .HasForeignKey(a => a.UserId)
            .WillCascadeOnDelete(false);

            this.Property(e => e.OrderState).IsRequired();

            this.Property(f => f.DateRecord).IsRequired();
                //.HasColumnType("datetime2");

            this.Property(f => f.DateViewRealEstate).IsRequired();
                //.HasColumnType("datetime2");
        }
    }
}
