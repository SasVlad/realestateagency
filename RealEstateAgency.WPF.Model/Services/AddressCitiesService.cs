﻿using RealEstateAgency.WPF.Model.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RealEstateAgency.WPF.Model.Models;
using RealEstateAgency.WPF.Model.Models.ModelDTO;

namespace RealEstateAgency.WPF.Model.Services
{
    public class AddressCitiesService : IAddressCityService
    {
        public async Task<List<AddressCityDTO>> GetAllCities()
        {
            return (await new SendToServerService<List<AddressCityDTO>, object>().GetDataByJsonObjectAsync("Address/GetAllCities")).Result;
        }
    }
}
