﻿using RealEstateAgency.WPF.Model.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RealEstateAgency.WPF.Model.Models;
using RealEstateAgency.WPF.Model.Models.ModelDTO;

namespace RealEstateAgency.WPF.Model.Services
{
    public class RealEstateStatusService : IRealEstateStatusService
    {
        public async Task<List<RealEstateStatusDTO>> GetAllRealEstateStatuses()
        {
            return (await new SendToServerService<List<RealEstateStatusDTO>, object>().GetDataByJsonObjectAsync("RealEstate/GetAllStatuses")).Result;
        }
    }
}
