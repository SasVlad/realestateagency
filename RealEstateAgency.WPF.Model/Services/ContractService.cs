﻿using RealEstateAgency.WPF.Model.Models.ModelDTO;
using RealEstateAgency.WPF.Model.Models.ModelFilters;
using RealEstateAgency.WPF.Model.Models.ModelViewDTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RealEstateAgency.WPF.Model.Services
{
    public class ContractService
    {
        public async Task<List<ContractViewDTO>> GetAllContractsView(string employeeId)
        {
            return (await new SendToServerService<List<ContractViewDTO>, object>().GetDataByJsonObjectAsync($"Contract/GetAllContractsView?employeeId={employeeId}")).Result;
        }
        public async Task<ResponsePackage<ContractDTO>> CreateContract(ContractDTO contractDto)
        {
            return await new SendToServerService<ContractDTO, ContractDTO>().PostDataByJsonObjectAsync("Contract/CreateContract", contractDto);
        }
        public async Task<List<ContractViewDTO>> FilterContractsRecord(ContractFilterModel contractFilterModel)
        {
            return (await new SendToServerService<List<ContractViewDTO>, ContractFilterModel>().PostDataByJsonObjectAsync("Contract/FilterContractView", contractFilterModel)).Result;
        }
    }
}
