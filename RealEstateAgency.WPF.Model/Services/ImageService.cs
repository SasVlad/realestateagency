﻿using RealEstateAgency.WPF.Model.Models;
using RealEstateAgency.WPF.Model.Models.ModelFilters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RealEstateAgency.WPF.Model.Services
{
    public class ImageService
    {
        public async Task<ResponsePackage<OperationDetails>> UploadImage(UploadImage image)
        {
            return await new SendToServerService<OperationDetails, UploadImage>().PostDataByJsonObjectAsync("Image/UploadImage", image);
        }
    }
}
