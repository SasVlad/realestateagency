﻿using RealEstateAgency.WPF.Model.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RealEstateAgency.WPF.Model.Models;
using RealEstateAgency.WPF.Model.Models.ModelFilters;
using RealEstateAgency.WPF.Model.Models.ModelDTO;

namespace RealEstateAgency.WPF.Model.Services
{
    public class AddressRegionServiceLocal//: IAddressRegionService
    {
        public ResponsePackage<List<AddressRegionDTO>> GetAllRegions()
        {
            return new ResponsePackage<List<AddressRegionDTO>>()
            {
                Result = new LocalRepository().GetRegions()
            };
        }
    };
}

