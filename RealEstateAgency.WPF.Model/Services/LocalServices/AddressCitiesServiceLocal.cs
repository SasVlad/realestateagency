﻿using RealEstateAgency.WPF.Model.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RealEstateAgency.WPF.Model.Models;
using RealEstateAgency.WPF.Model.Models.ModelDTO;
using RealEstateAgency.WPF.Model.Models.ModelFilters;

namespace RealEstateAgency.WPF.Model.Services
{
    public class AddressCitiesServiceLocal //: IAddressRegionService
    {
        public ResponsePackage<List<AddressCityDTO>> GetAllCities()
        {
            return new ResponsePackage<List<AddressCityDTO>>() { Result = new LocalRepository().GetCity() };
        }
    }
}
