﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using RealEstateAgency.WPF.Model.Interfaces;
using RealEstateAgency.WPF.Model.Models;
using RealEstateAgency.WPF.Model.Models.ModelDTO;
using RealEstateAgency.WPF.Model.Models.ModelFilters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace RealEstateAgency.WPF.Model.Services
{
    public class AccountService: IAccountService
    {                       
        public async Task<ResponsePackage<EmployeeDTO>> IsAuthorized(LoginDto loginModel)
        {
            return await new SendToServerService<EmployeeDTO, LoginDto>().PostDataByJsonObjectAsync("Account/LoginWPF", loginModel);
        }
    }
}
