﻿using RealEstateAgency.WPF.Model.Models;
using RealEstateAgency.WPF.Model.Models.ModelDTO;
using RealEstateAgency.WPF.Model.Models.ModelFilters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RealEstateAgency.WPF.Model.Interfaces
{
    public interface IAddressService
    {
        Task<List<AddressDTO>> GetAllAddresses();
        Task<ResponsePackage<AddressDTO>> UpdateAddressRecord(AddressDTO address);
    }
}
