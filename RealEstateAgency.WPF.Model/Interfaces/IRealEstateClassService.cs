﻿using RealEstateAgency.WPF.Model.Models;
using RealEstateAgency.WPF.Model.Models.ModelDTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RealEstateAgency.WPF.Model.Interfaces
{
    public interface IRealEstateClassService
    {
        Task<List<RealEstateClassDTO>> GetAllRealEstateClasses();
    }
}
