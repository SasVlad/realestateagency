﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RealEstateAgency.WPF.Model.Models.ModelFilters
{
    public class AddressStreetFilterModel
    {
        public int? AddressStreetID { get; set; }
        public string AddressStreetName { get; set; }
    }
}
