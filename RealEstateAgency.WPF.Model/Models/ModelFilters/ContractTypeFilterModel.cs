﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RealEstateAgency.WPF.Model.Models.ModelFilters
{
    public class ContractTypeFilterModel
    {
        public int? ContractTypeID { get; set; }
        public string ContractTypeName { get; set; }
    }
}
