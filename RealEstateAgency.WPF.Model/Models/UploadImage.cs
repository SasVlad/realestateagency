﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RealEstateAgency.WPF.Model.Models
{
    public class UploadImage
    {
        public string ImageBase64 { get; set; }
        public string ImageName { get; set; }
    }
}
