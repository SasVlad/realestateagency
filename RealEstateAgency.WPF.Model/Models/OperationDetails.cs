﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RealEstateAgency.WPF.Model.Models
{
    public class OperationDetails
    {
        public OperationDetails(bool successed, string message, string prop, string id = "")
        {
            Successed = successed;
            Message = message;
            Property = prop;
            Id = id;
        }
        public bool Successed { get; private set; }
        public string Message { get; private set; }
        public string Property { get; private set; }
        public string Id { get; private set; }
    }
}
