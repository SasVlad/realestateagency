import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { RealEstateType } from '../../models/real-estate-view';
import { RealEstateDataStoreService } from '../../shared/data-stores/real-estate-data-store.service';
import { RealEstateService } from '../../shared/real-estate.service';
import { RealEstateFilterView } from '../../models/real-estate-filter-view';

@Component({
    selector: 'header-menu',
    templateUrl: './header.component.html'
})

export class HeaderComponent implements OnInit  {

    // login blogin login
    public LoginStatus: boolean = false;
    public isEmployeeRole: boolean = false;
  
  public isShowLoginDialog: boolean = false;
  public realEstateTypeList: RealEstateType[] = [];
  public realEstateFilterView: RealEstateFilterView = new RealEstateFilterView();

  ngOnInit() {
    this.realEstateService.getRealEstateTypes().subscribe((result) => {
      this.realEstateTypeList = result.Result;
    });
    //this.realEstateDataStoreService.typesList.subscribe(typesList => {
    //  this.realEstateTypeList = typesList
    //});
  }

  constructor(private realEstateService: RealEstateService,
    private router: Router) {
        router.events.subscribe((val) => {
            this.checkLogInUser();
        });
        

      }
      collapse: string = 'collapse';

      collapseNavbar(): void {
          if (this.collapse.length > 1) {
              this.collapse = '';
          } else {
              this.collapse = 'collapse';
          }
      }
      userSuccessSignIn($event) {
        this.isShowLoginDialog = $event;
        this.LoginStatus = true;
        this.router.navigate(["my-profile"]);
      }
      collapseMenu() {
          this.collapse = 'collapse';
      }
      checkLogInUser() {
          var userGuid = localStorage.getItem('personGuid');
          this.LoginStatus = userGuid != null;
          this.isEmployeeRole = localStorage.getItem('personRole') != "User";
      }
  logout() {
    localStorage.removeItem('personGuid');
    localStorage.removeItem('personRole');
    this.router.navigate(["home"]);
  }
  public redirectToFilterRealEstate() {
    localStorage.setItem('filterModel', JSON.stringify(this.realEstateFilterView));
    this.router.navigate(["filter"]);
  }
  public filterByRealEstateType(typeId: number) {
    this.realEstateFilterView.RealEstateFilter.RealEstateTypeID = typeId;
    this.redirectToFilterRealEstate();
  }
  public closeModal() {
    this.isShowLoginDialog = !this.isShowLoginDialog;
  }
}
