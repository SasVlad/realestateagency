import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ImageHelper } from '../../helpers/image-helper';
import { User, UserView } from '../../models/user-view';
import { Employee, EmployeeView } from '../../models/employee-view';

@Component({
    selector: 'app-nav-menu',
    templateUrl: './navmenu.component.html',
    styleUrls: ['./navmenu.component.css']
})

export class NavMenuComponent implements OnInit {
     @Input() isEmployeeRole: boolean = false;
     @Input() currentPersonModel: UserView | EmployeeView;
      public userRole: string;
    constructor(private router: Router,
      private imageHelper: ImageHelper) {
      this.userRole = localStorage.getItem('personRole');     
    }
    collapse: string = 'collapse';

    collapseNavbar(): void {
        if (this.collapse.length > 1) {
            this.collapse = '';
        } else {
            this.collapse = 'collapse';
        }
    }
    ngOnInit() {
    }
    collapseMenu() {
        this.collapse = 'collapse';
    }

    logout() {
        localStorage.removeItem('personGuid');
        localStorage.removeItem('personRole');
        this.router.navigate(["home"]);
    }
}
