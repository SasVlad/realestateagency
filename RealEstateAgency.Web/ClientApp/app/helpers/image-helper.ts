import { Injectable, Inject, Injector } from '@angular/core';
import { REQUEST } from '@nguniversal/aspnetcore-engine/tokens';

@Injectable()
export class ImageHelper {
  private baseUrl: string;
  constructor(private injector: Injector) {
    this.baseUrl = this.injector.get(REQUEST);
  }
  getUserImage(imageString: string) {
    return !imageString ? `${this.baseUrl}/images/no-image.gif` : `${this.baseUrl}/images/${imageString}`;
  }
  getRealEstateImages(imagesString: string) {
    if (imagesString == null) {
      return `${this.baseUrl}/images/no-image.gif`;
    }
    var imageUrls = [];

    imagesString.split('|').forEach(image => imageUrls.push(`${this.baseUrl}/images/${image}`));
    return imageUrls;
  }
}
