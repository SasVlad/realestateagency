export class Order {
  public Id: number = null;
  public UserId: string = null;
  public EmployeeId: string = null;
  public RealEstateId: number = null;
  public DateViewRealEstate: string = null;
  public TimeDateViewRealEstate: string = null;
  public DateRecord: string = null;
  public OrderState: OrderState = null;
}
export enum OrderState {
  Confirmed,
  NotConfirmed,
  Cancelled,
  InReview
}
export class OrderStatusText {
  orderStateEnumValue: OrderState;
  orderStateTextValue: string;
}
