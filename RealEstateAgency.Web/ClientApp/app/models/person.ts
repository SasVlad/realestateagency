export class Person {
  public PersonId: string = null;
  public Name: string = null;
  public Surname: string = null;
  public Patronumic: string = "";
  public PhoneNumber: number = 0;
  public PassportNumber: string = "";
  public AddressID: number = 0;
  public Role: string = null;
  public Email: string = null;
  public Password: string = null;
  public ImageUrl: string=null;
}
