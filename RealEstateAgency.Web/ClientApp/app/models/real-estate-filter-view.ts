import { AddressFilter } from '../models/address-filter';

export class RealEstateFilterView {
  public RealEstateFilter: RealEstateFilter = new RealEstateFilter();
  public AddressFilter: AddressFilter = new AddressFilter();
}
export class RealEstateFilter {
  public RealEstateID: number = null;
  public RealEstateStatusID: number = null;
  public RealEstateTypeID: number=null;
  public BeginPrice: number = null;
  public EndPrice: number = null;
  public RealEstateClassID: number = null;
  public RealEstateTypeWallID: number = null;
  public BeginNumberOfRooms: number = null;
  public EndNumberOfRooms: number = null;
  public BeginLevel: number = null;
  public EndLevel: number = null;
  public BeginGrossArea: number = null;
  public EndGrossArea: number = null;
  public NearSubway: string=null;
  public Elevator: boolean = null;
  public AddressID: number = null;  
}
