export class AddressFilter {
  public AddressID: number = null;
  public AddressCityID: number = null;
  public AddressRegionID: number = null;
  public AddressStreetID: number = null;
  public HomeNumber : string=null;
  public ApartmentNumber: number = null;
}
