import { Person } from '../models/person';
import { AddressView } from '../models/address-view';

export class UserView {
  Person: User = new User();
  AddressView: AddressView = new AddressView();
}
export class User extends Person {
}
