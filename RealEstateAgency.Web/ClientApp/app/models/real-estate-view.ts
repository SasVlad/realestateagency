import { AddressView } from '../models/address-view';
import { Employee } from '../models/employee-view';

export class RealEstateView {
  public RealEstate: RealEstate = new RealEstate();
  public RealEstateClass: RealEstateClass = new RealEstateClass();
  public RealEstateStatus: RealEstateStatus = new RealEstateStatus();
  public RealEstateType: RealEstateType = new RealEstateType();
  public RealEstateTypeWall: RealEstateTypeWall = new RealEstateTypeWall();
  public AddressView: AddressView = new AddressView();
  public Employee: Employee = new Employee();
}

export class RealEstate {
  public RealEstateID: number;
  public RealEstateStatusID : number;
  public RealEstateTypeID : number;
  public Price : number;
  public RealEstateClassID : number;
  public RealEstateTypeWallID : number;
  public NumberOfRooms : number;
  public Level : number;   
  public GrossArea : number;
  public NearSubway: string; 
  public Elevator : boolean;
  public AddressID : number;
  public EmployeeId : string; 
  public ImageUrls : string; 
}

export class RealEstateClass {
  public RealEstateClassID : number;
  public RealEstateClassName: string;
}

export class RealEstateStatus {
  public RealEstateStatusID : number;
  public RealEstateStatusName : string;
}

export class RealEstateType {
  public RealEstateTypeID : number;
  public RealEstateTypeName: string;
  public ImageUrl: string;
}

export class RealEstateTypeWall {
  public RealEstateTypeWallID : number;
  public RealEstateTypeWallName : string;
}

