import { Person } from '../models/person';
import { AddressView } from './address-view';

export class EmployeeView {
  Person: Employee = new Employee();
  AddressView: AddressView = new AddressView();
  Dismisses = [];
  Post = new EmployeePost();
}

export class Employee extends Person{
  public EmployeePostID: number = 0;
  public EmployeeStatusID: number = 0;
  public StateOnline: boolean = false;
}
export class EmployeeDismiss{
  public EmployeeDismissId: number = 0;
  public EmployeeId: string = "";
  public EmploymentDate: Date;
  public DismissDate: Date;
}
export class EmployeePost{
  public EmployeePostID: number = 0;
  public EmployeePostName: string = "";
  public EmployeePostSalary: number = 0;
}
