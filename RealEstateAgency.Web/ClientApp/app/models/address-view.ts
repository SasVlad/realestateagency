export class AddressView {
  public Address: Address = new Address();
  public AddressCity: AddressCity = new AddressCity();
  public AddressRegion: AddressRegion = new AddressRegion();
  public AddressStreet: AddressStreet = new AddressStreet();
}
export class Address {
  public AddressID: number = 0;
  public AddressCityID : number=null;
  public AddressRegionID: number = null;
  public AddressStreetID: number = null;
  public HomeNumber: string = null;
  public ApartmentNumber: number = null;
}
export class AddressCity {
  public AddressCityID: number = null;
  public AddressCityName: string = null;
}
export class AddressRegion {
  public AddressRegionID: number = null;
  public AddressRegionName: string = null;
}
export class AddressStreet {
  public AddressStreetID: number = null;
  public AddressStreetName: string = null;
}
