import { Order } from '../models/order';
import { UserView } from '../models/user-view';
import { RealEstateView } from '../models/real-estate-view';
import { Employee, EmployeeView } from '../models/employee-view';

export class OrderView {
  public Order: Order = new Order();
  public UserView: UserView = new UserView();
  public RealEstateView: RealEstateView = new RealEstateView();
  public EmployeeView: EmployeeView = new EmployeeView();
  public isOverView: boolean = false;
}
