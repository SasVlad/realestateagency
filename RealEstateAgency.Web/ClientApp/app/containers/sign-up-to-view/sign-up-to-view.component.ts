import { Component, OnInit } from '@angular/core';
import { RealEstateDataStoreService } from '../../shared/data-stores/real-estate-data-store.service';
import { RealEstate, RealEstateView } from '../../models/real-estate-view';
import { ImageHelper } from '../../helpers/image-helper';
import { UserDataStoreService } from '../../shared/data-stores/user-data-store.service';
import { UserView } from '../../models/user-view';
import { OrderService } from '../../shared/order.service';
import { Order, OrderState } from '../../models/order';
import { Router } from '@angular/router';

@Component({
  selector: 'sign-up-to-view',
  templateUrl: './sign-up-to-view.component.html'
})
export class SignUpToViewComponent implements OnInit {

  public LoginStatus: boolean = false;
  public showModalUnsver: boolean = false;
  public showModalUnsverStatus: boolean = false;

  public isShowLoginDialog: boolean = false;

  public selectedRealEstate: RealEstateView;
  public selectedUser: UserView;
  public orderModel: Order = new Order();
  public freeTimesToDate: number[] = [];
  constructor(private imageHelper: ImageHelper,
    private router: Router,
    private realEstateDataStoreService: RealEstateDataStoreService,
    private userDataStoreService: UserDataStoreService,
    private orderService:OrderService) {
    this.checkLogInUser();
    realEstateDataStoreService.realEstateForOrder.subscribe(realEstate => {
      this.selectedRealEstate = realEstate;
    })
    userDataStoreService.currentUser.subscribe(user => {
      this.selectedUser = user;
    })
    this.initTimesArray();
  }

  ngOnInit() { }
  public closeModal() {
    this.isShowLoginDialog = !this.isShowLoginDialog;
  }
  checkLogInUser() {
    var userGuid = localStorage.getItem('personGuid');
    this.LoginStatus = userGuid != null;
  }
  userSuccessSignIn($event) {
    this.isShowLoginDialog = $event;
    this.LoginStatus = true;
  }
  getTimeToOrderByDate() {
    this.orderService.getOrderBusyTimeByDate(this.orderModel.DateViewRealEstate, this.selectedRealEstate.RealEstate.RealEstateID).subscribe(result => {
      this.initTimesArray();
      this.freeTimesToDate = this.freeTimesToDate.filter(function (el) {
        return result.Result.indexOf(el) < 0;
      });
      //this.router.navigate(["home"]);
    });
  }
  submitOrder() {
    this.orderModel.EmployeeId = this.selectedRealEstate.Employee.PersonId;
    this.orderModel.RealEstateId = this.selectedRealEstate.RealEstate.RealEstateID;
    this.orderModel.UserId = this.selectedUser.Person.PersonId;
    this.orderModel.DateRecord = new Date().toISOString().slice(0, 10);;
    this.orderModel.OrderState = OrderState.NotConfirmed;
    this.orderService.createOrder(this.orderModel).subscribe(result => {
      this.router.navigate(["home"]);
    });
  }
  initTimesArray() {
    this.freeTimesToDate = [9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20];
  }
}
