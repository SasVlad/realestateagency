import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { User } from '../../models/user-view';
import { UserService } from '../../shared/user.service';
import { Router } from '@angular/router';

@Component({
    selector: 'registration',
    templateUrl: './registration.component.html'
})
export class RegistrationComponent implements OnInit {
    @Output()
    signInUser: EventEmitter<boolean> = new EventEmitter<boolean>();
    public newUser: User = new User();
    constructor(private userService: UserService,
    private router: Router) { }

    ngOnInit() { }
    SignUp() {
        this.newUser.Role = "User";
      this.userService.createUser(this.newUser).subscribe(result => {
        //$("#login-modal").show();
        if (result.Result.Successed) {
          this.signInUser.emit(false);
          localStorage.setItem('personGuid', result.Result.Id);
          localStorage.setItem('personRole', this.newUser.Role);
        }
        console.log(result.Result.Message)

      });
    }
}
