import { Component, ElementRef, OnInit, Inject, Injector, ViewChild } from '@angular/core';

import { TranslateService } from '@ngx-translate/core';
import { RealEstateService } from '../../shared/real-estate.service';
import { RealEstateView, RealEstateType, RealEstateTypeWall, RealEstateClass } from '../../models/real-estate-view';
import { AddressView, AddressCity, AddressRegion, AddressStreet } from '../../models/address-view';
import { AddressService } from '../../shared/address.service';
import { RealEstateFilterView } from '../../models/real-estate-filter-view';
import { Router } from '@angular/router';
import { ImageHelper } from '../../helpers/image-helper';
import { AddressDataStoreService } from '../../shared/data-stores/address-data-store.service';
import { RealEstateDataStoreService } from '../../shared/data-stores/real-estate-data-store.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html'
})
//li--width: 100%;float: left;margin-right: -100%;position: relative;opacity: 0;display: block;z-index: 1;
export class HomeComponent implements OnInit {
  public carouselRealEstates: RealEstateView[];

  public isShowModal: boolean = false;
  public istabImge: number = 0;
  public selectRealEstate: RealEstateView;

  //----search console
  public realEstateTypeList: RealEstateType[] = [];
  public realEstateTypeWallList: RealEstateTypeWall[] = [];
  public realEstateClassList: RealEstateClass[] = [];

  public addressCityList: AddressCity[] = [];
  public addressRegionList: AddressRegion[] = [];
  public addressStreetList: AddressStreet[] = [];

  public realEstateFilterView: RealEstateFilterView = new RealEstateFilterView();
  
  public isShortSearchConsole: boolean = true;
  public isEmployeeRole: boolean = false;
  public viewFullProduct(selectIndex: number) {
    this.selectRealEstate = this.carouselRealEstates[selectIndex];
    this.isShowModal = true;
    this.istabImge = 0;
  }
  public closeModal() {
    this.isShowModal = !this.isShowModal;
    this.istabImge = 0;
  }
  public tabImge(indexImge) {
    this.istabImge = indexImge;
  }

  constructor(private realEstateService: RealEstateService,
    private addressService: AddressService,
      private router: Router,
    private imageHelper: ImageHelper,
    private addressDataStoreService: AddressDataStoreService,
    private realEstateDataStoreService: RealEstateDataStoreService) {
  }
  ngOnInit() {
    let userRole = localStorage.getItem('personRole');
    this.isEmployeeRole = userRole != "User";
    this.realEstateService.getRealEstates().subscribe((result) => {
      this.carouselRealEstates = result.Result;
    });
    this.realEstateDataStoreService.typesList.subscribe(typesList => {
      this.realEstateTypeList = typesList
    });
    this.realEstateDataStoreService.typeWallsList.subscribe(typeWallsList => {
      this.realEstateTypeWallList = typeWallsList
    });
    this.realEstateDataStoreService.classesList.subscribe(classesList => {
      this.realEstateClassList = classesList
    });
    this.addressDataStoreService.citiesList.subscribe(citiesList => {
      this.addressCityList = citiesList
    });
    this.addressDataStoreService.regionsList.subscribe(regionsList => {
      this.addressRegionList = regionsList
    });
    this.addressDataStoreService.streetsList.subscribe(streetsList => {
      this.addressStreetList = streetsList
    });
  }

  getRealEstateAddress(addressView: AddressView) {
    var street = addressView.AddressStreet.AddressStreetName;
    var city = addressView.AddressCity.AddressCityName;
    var region = addressView.AddressRegion.AddressRegionName;
    return `${city}, ${region}, ${street}, ${addressView.Address.HomeNumber}`;
  }
  public redirectToFilterRealEstate() {
    localStorage.setItem('filterModel', JSON.stringify(this.realEstateFilterView));
    this.router.navigate(["filter"]);
  }
  public filterByRealEstateType(typeId: number) {
    this.realEstateFilterView.RealEstateFilter.RealEstateTypeID = typeId;
    this.redirectToFilterRealEstate();
  }
  public selectRealEstateToOrder() {
    this.realEstateDataStoreService.setRealEstateForOrder(this.selectRealEstate);
    localStorage.setItem('realEstateForOrderGuid', this.selectRealEstate.RealEstate.RealEstateID.toString());
    this.router.navigate(["sign-up-to-view"]);
  }
}
