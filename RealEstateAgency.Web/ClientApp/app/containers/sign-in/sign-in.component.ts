import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { Login } from '../../models/login';
import { AccountService } from '../../shared/account.service';
import { Router } from '@angular/router';

@Component({
    selector: 'sign-in',
    templateUrl: './sign-in.component.html'
})
export class SignInComponent implements OnInit {
    public loginModel: Login = new Login();
    @Output()
    signInUser: EventEmitter<boolean> = new EventEmitter<boolean>();
    constructor(private accountService: AccountService,
      private router: Router) {
    }

    ngOnInit() { }
    SignIn() {
      this.accountService.Login(this.loginModel).subscribe(result => {
        if (result.Result) {
          this.signInUser.emit(false);
            localStorage.setItem('personGuid', result.Result.Id);
            localStorage.setItem('personRole', result.Result.Role);           
        }
        console.log(result.Result.Message)

      });
    }
}
