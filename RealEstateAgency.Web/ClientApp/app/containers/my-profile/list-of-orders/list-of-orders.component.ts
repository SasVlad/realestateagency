import { Component, OnInit, Injector } from '@angular/core';
import { User, UserView } from '../../../models/user-view';
import { Employee, EmployeeView } from '../../../models/employee-view';
import { UserService } from '../../../shared/user.service';
import { EmployeeService } from '../../../shared/employee.service';
import { REQUEST } from '@nguniversal/aspnetcore-engine/tokens';
import { OrderDataStoreService } from '../../../shared/data-stores/order-data-store.service';
import { OrderView } from '../../../models/order-view';
import { AddressView } from '../../../models/address-view';
import { ImageHelper } from '../../../helpers/image-helper';
import { ResponseSignInModel } from '../../../models/response-sign-in-model';
import { OrderService } from '../../../shared/order.service';
import { OrderStatusText, OrderState } from '../../../models/order';

@Component({
  selector: 'list-of-orders',
  templateUrl: './list-of-orders.component.html'
})
export class ListOfOrdersComponent implements OnInit {
  
  public isEmployeeRole: boolean = false; //true == admin | false == user
  public clickMore: boolean = false; //status in array | true/false
  public currentPersonModel: UserView | EmployeeView;
  private baseUrl: string;
  private ordersList: OrderView[] = []
  private selectedOrder: OrderView = new OrderView();
  private orderStatusDictionary: OrderStatusText[] = [];

  constructor(private imageHelper: ImageHelper,
    private injector: Injector,
    private userService: UserService,
    private employeeService: EmployeeService,
    private orderDataStoreService: OrderDataStoreService,
    private orderService: OrderService  ) {

    let userGuid = localStorage.getItem('personGuid');
    let userRole = localStorage.getItem('personRole');
    this.isEmployeeRole = userRole != "User";

    let personOrderModel = new ResponseSignInModel();
    personOrderModel.Id = userGuid;
    personOrderModel.Role = userRole;
    this.orderService.getOrdersView(personOrderModel).subscribe(ordersList => {
      if (ordersList.Result != null) {
        this.ordersList = ordersList.Result;
      }
      //console.log("user not found");
    })
    this.initOrderStateDictionary();
    //this.orderDataStoreService.ordersList.subscribe(ordersList => {
    //  this.ordersList = ordersList;
    //  console.log(this.ordersList);
    //});

  }
  initOrderStateDictionary() {
    this.orderStatusDictionary.push({ orderStateEnumValue: OrderState.NotConfirmed, orderStateTextValue: "Новый" })
    this.orderStatusDictionary.push({ orderStateEnumValue: OrderState.Confirmed, orderStateTextValue: "Подтвержденный" })
    this.orderStatusDictionary.push({ orderStateEnumValue: OrderState.InReview, orderStateTextValue: "На просмотре" })
    this.orderStatusDictionary.push({ orderStateEnumValue: OrderState.Cancelled, orderStateTextValue: "Отмененный" })
  }
  getRealEstateAddress(addressView: AddressView) {
    var street = addressView.AddressStreet.AddressStreetName;
    var city = addressView.AddressCity.AddressCityName;
    var region = addressView.AddressRegion.AddressRegionName;
    return `${city}, ${region}, ${street}, ${addressView.Address.HomeNumber}`;
  }
  ngOnInit() {

  }

  overviewOrder(order: OrderView) {
    order.isOverView = !order.isOverView;
    this.selectedOrder = order;
  }
  getOrderStatusText(orderEnumValue: OrderState) {
    return this.orderStatusDictionary.find(x => x.orderStateEnumValue == orderEnumValue).orderStateTextValue;
  }
  updateStateInOrder($event: OrderState) {
    this.selectedOrder.Order.OrderState = $event;
    this.orderService.updateOrder(this.selectedOrder.Order).subscribe(result => {
      console.log("success update order status");
    });

  }
}
