import { Component, Injector } from '@angular/core';
import { EmployeeService } from '../../shared/employee.service';
import { UserService } from '../../shared/user.service';
import { User, UserView } from '../../models/user-view';
import { REQUEST } from '@nguniversal/aspnetcore-engine/tokens';
import { Employee, EmployeeView } from '../../models/employee-view';
import { Router } from '@angular/router';
import { UserDataStoreService } from '../../shared/data-stores/user-data-store.service';
import { AddressView } from '../../models/address-view';

@Component({
    selector: 'my-profile',
    templateUrl: './my-profile.component.html'
})
export class MyProfileComponent {
  public isEmployeeRole: boolean = false; //true == admin | false == user
  public clickMore: boolean = false; //status in array | true/false
  public currentPersonModel: UserView | EmployeeView;
  private baseUrl: string;

  constructor(private injector: Injector,
    private userService: UserService,
    private employeeService: EmployeeService,
    private router: Router,
    private userDataStoreService: UserDataStoreService) {

    this.baseUrl = this.injector.get(REQUEST);
    let userGuid = localStorage.getItem('personGuid');
    let userRole = localStorage.getItem('personRole');
    if (userGuid == null) {
      this.logout()
    }
    this.isEmployeeRole = userRole != "User";
    this.getPerson(userGuid, userRole);
  }
  getPerson(guid: string, userRole: string) {
    if (userRole == "User") {
      this.userService.getUserView(guid).subscribe(user => {
        if (user.Result != null) {
          if (user.Result.AddressView == null) {
            user.Result.AddressView = new AddressView();
          }
          this.currentPersonModel = user.Result;
          this.userDataStoreService.setUser(this.currentPersonModel); 
          return;
        }
        console.log("user not found");
      })
    }
    if (userRole != "User") {
      this.employeeService.getEmployeeView(guid).subscribe(employee => {
        if (employee.Result != null) {
          if (employee.Result.AddressView==null) {
            employee.Result.AddressView = new AddressView();
          }
          this.currentPersonModel = employee.Result;
          this.userDataStoreService.setUser(this.currentPersonModel); 
          return;
        }
        console.log("user not found");
      })
    }
  }
  logout() {
    localStorage.removeItem('personGuid');
    localStorage.removeItem('personRole');
    this.router.navigate(["home"]);
  }

}
