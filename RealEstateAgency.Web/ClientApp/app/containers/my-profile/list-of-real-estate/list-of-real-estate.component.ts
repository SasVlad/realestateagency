import { Component } from '@angular/core';
import { RealEstateService } from '../../../shared/real-estate.service';
import { RealEstateView } from '../../../models/real-estate-view';
import { AddressView } from '../../../models/address-view';
import { ImageHelper } from '../../../helpers/image-helper';

@Component({
    selector: 'app-list-of-real-estate',
    templateUrl: './list-of-real-estate.component.html'
})
export class ListOfRealEstateComponent {

    public clickMore: boolean = false;
    public realEstatesList: RealEstateView[];
    public selectRealEstate: RealEstateView;
    public istabImge: number = 0;
  constructor(private imageHelper: ImageHelper,
    private realEstateService: RealEstateService) {
    let userGuid = localStorage.getItem('personGuid');
    realEstateService.getRealEstatesByEmployeeId(userGuid).subscribe(result => {
      this.realEstatesList = result.Result;
    })
    }
  getRealEstateAddress(addressView: AddressView) {
    var street = addressView.AddressStreet.AddressStreetName;
    var city = addressView.AddressCity.AddressCityName;
    var region = addressView.AddressRegion.AddressRegionName;
    return `${city}, ${region}, ${street}, ${addressView.Address.HomeNumber}`;
  }
  public overviewRealEstate(realEstate: RealEstateView) {
    this.selectRealEstate = realEstate;
    this.clickMore = !this.clickMore;
    this.istabImge = 0;
  }
  public closeModal() {
    this.clickMore = !this.clickMore;
  }
  public tabImge(indexImge) {
    this.istabImge = indexImge;
  }
}
