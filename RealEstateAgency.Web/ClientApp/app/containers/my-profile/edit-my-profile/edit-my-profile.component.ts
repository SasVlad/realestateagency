import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { User, UserView } from '../../../models/user-view';
import { Employee, EmployeeView } from '../../../models/employee-view';
import { UserDataStoreService } from '../../../shared/data-stores/user-data-store.service';
import { AddressDataStoreService } from '../../../shared/data-stores/address-data-store.service';
import { AddressCity, AddressRegion, AddressStreet } from '../../../models/address-view';
import { AddressService } from '../../../shared/address.service';
import { UserService } from '../../../shared/user.service';
import { EmployeeService } from '../../../shared/employee.service';
import { ResetPasswordModel } from '../../../models/reset-passwod-model';
import { AccountService } from '../../../shared/account.service';
import { ImageService } from '../../../shared/image.service';
import { UploadImage } from '../../../models/upload-image-model';

@Component({
  selector: 'app-edit-myprofile',
  templateUrl: './edit-my-profile.component.html'
})
export class EditMyProfileComponent implements OnInit {
  public isEmployeeRole: boolean = false;
  public currentPersonModel: UserView | EmployeeView;

  public addressCityList: AddressCity[] = [];
  public addressRegionList: AddressRegion[] = [];
  public addressStreetList: AddressStreet[] = [];

  @ViewChild('fileInput') fileInput;
  public resetPasswordModel: ResetPasswordModel = new ResetPasswordModel();
  public isSamePasswords = true;
  public isReqiredFieldsNotEmty = true;
  public uploadImageModel: UploadImage = new UploadImage();
  constructor(private userDataStoreService: UserDataStoreService,
    private addressDataStoreService: AddressDataStoreService,
    private addressService: AddressService,
    private userService: UserService,
    private employeeService: EmployeeService,
    private accountService: AccountService,
    private imageService: ImageService
  ) {
    this.getUser();
    this.isEmployeeRole = localStorage.getItem('personRole') != "User";
    this.addressDataStoreService.citiesList.subscribe(citiesList => {
      this.addressCityList = citiesList
    });
    this.addressDataStoreService.regionsList.subscribe(regionsList => {
      this.addressRegionList = regionsList
    });
    this.addressDataStoreService.streetsList.subscribe(streetsList => {
      this.addressStreetList = streetsList
    });
  }

  ngOnInit() { }
  getUser() {
    this.userDataStoreService.currentUser.subscribe(user => {
      this.currentPersonModel = user
    }, error => {
      console.log(error);
    });
  }
  handleInputChange(e) {
    var file = e.dataTransfer ? e.dataTransfer.files[0] : e.target.files[0];
    var pattern = /image-*/;
    var reader = new FileReader();

    if (!file.type.match(pattern)) {
      alert('invalid format');
      return;
    }
    this.uploadImageModel.ImageName = file.name;
    reader.onload = this._handleReaderLoaded.bind(this);
    reader.readAsDataURL(file);
  }
  _handleReaderLoaded(e) {
    var reader = e.target;
    this.uploadImageModel.ImageBase64 = reader.result;
    
    this.imageService.uploadImage(this.uploadImageModel).subscribe(result => {
      this.currentPersonModel.Person.ImageUrl = result.Result.Message;
      if (this.isEmployeeRole) {
        this.updateEmployeeData();
        return;
      }
      this.updateUserData();
    })
  }
  savePersonDataChanges() {
    if (this.isEmployeeRole) {
      this.saveEmployeeDataChanges();
      return;
    }
    this.saveUserDataChanges();
  }

  saveUserDataChanges() {
    if (this.currentPersonModel.Person.AddressID == 0) {
      this.updateUserDataWithoutAddress();
      return;
    }
    this.updateUserDataWithAddress();
    console.log(this.currentPersonModel.Person.AddressID);
  }

  updateUserDataWithoutAddress() {
    this.addressService.createAddress(this.currentPersonModel.AddressView.Address).subscribe(result => {
      this.currentPersonModel.Person.AddressID = result.Result.Id;
      this.userService.updateUser(this.currentPersonModel.Person).subscribe(result => {
        console.log("update WithoutAddress success");
      })
    })
  }

  updateUserDataWithAddress() {
    this.updateUserData();
    this.updateAddressData();
  }
  
  saveEmployeeDataChanges() {
    if (this.currentPersonModel.Person.AddressID == 0) {
      this.updateEmployeeDataWithoutAddress();
      return;
    }
    this.updateEmployeeDataWithAddress();
    console.log(this.currentPersonModel.Person.AddressID);
  }
  updateEmployeeDataWithoutAddress() {
    this.addressService.createAddress(this.currentPersonModel.AddressView.Address).subscribe(result => {
      this.currentPersonModel.Person.AddressID = result.Result.Id;
      this.employeeService.updateEmployee(<Employee>this.currentPersonModel.Person).subscribe(result => {
        console.log("update WithoutAddress success");
      })
    }) 
  }
  updateEmployeeDataWithAddress() {
    this.updateEmployeeData();
    this.updateAddressData();
  }
  updateUserData() {
    this.userService.updateUser(this.currentPersonModel.Person).subscribe(result => {
      console.log("update User success");
    })
  }
  updateEmployeeData() {
    this.employeeService.updateEmployee(<Employee>this.currentPersonModel.Person).subscribe(result => {
      console.log("update Employee success");
    })
  }
  updateAddressData() {
    this.addressService.updateAddress(this.currentPersonModel.AddressView.Address).subscribe(result => {
      console.log("update Address success");
    });
  }
  resetPassword() {
    this.isSamePasswords = this.resetPasswordModel.NewPassword == this.resetPasswordModel.RepeateNewPassword;
    if (this.isSamePasswords) {
      this.resetPasswordModel.UserID = this.currentPersonModel.Person.PersonId;
      this.accountService.resetPassword(this.resetPasswordModel).subscribe(result => {
        this.resetPasswordModel = new ResetPasswordModel();
      })
    }
    
  }
}
