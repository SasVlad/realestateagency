import { Component, Injector } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { RealEstateService } from '../../shared/real-estate.service';
import { AddressView, AddressCity, AddressRegion, AddressStreet } from '../../models/address-view';
import { RealEstateView, RealEstateType, RealEstateTypeWall, RealEstateClass } from '../../models/real-estate-view';
import { RealEstateFilterView } from '../../models/real-estate-filter-view';
import { AddressService } from '../../shared/address.service';
import { ImageHelper } from '../../helpers/image-helper';
import { RealEstateDataStoreService } from '../../shared/data-stores/real-estate-data-store.service';
import { AddressDataStoreService } from '../../shared/data-stores/address-data-store.service';
@Component({
  selector: 'app-filter-view',
  templateUrl: './filter-view.component.html'
})
export class FilterViewComponent {
  public isShowModal: boolean = false;
  public istabImge: number = 0;
  private realEstateList: RealEstateView[];
  public selectRealEstate: RealEstateView;
  public realEstateFilterView: RealEstateFilterView = new RealEstateFilterView();
  public realEstateTypeList: RealEstateType[] = [];
  public realEstateTypeWallList: RealEstateTypeWall[] = [];
  public realEstateClassList: RealEstateClass[] = [];

  public addressCityList: AddressCity[] = [];
  public addressRegionList: AddressRegion[] = [];
  public addressStreetList: AddressStreet[] = [];
  public isEmployeeRole: boolean = false;
  constructor(private route: ActivatedRoute,
    private realEstateService: RealEstateService,    
    private addressService: AddressService,
    private router: Router,
    private imageHelper: ImageHelper,
    private realEstateDataStoreService: RealEstateDataStoreService,
    private addressDataStoreService: AddressDataStoreService) {
    let userRole = localStorage.getItem('personRole');
    this.isEmployeeRole = userRole != "User";
    this.realEstateDataStoreService.typesList.subscribe(typesList => {
      this.realEstateTypeList = typesList
    });
    this.realEstateDataStoreService.typeWallsList.subscribe(typeWallsList => {
      this.realEstateTypeWallList = typeWallsList
    });
    this.realEstateDataStoreService.classesList.subscribe(classesList => {
      this.realEstateClassList = classesList
    });
    this.addressDataStoreService.citiesList.subscribe(citiesList => {
      this.addressCityList = citiesList
    });
    this.addressDataStoreService.regionsList.subscribe(regionsList => {
      this.addressRegionList = regionsList
    });
    this.addressDataStoreService.streetsList.subscribe(streetsList => {
      this.addressStreetList = streetsList
    });
    this.realEstateFilterView = JSON.parse(localStorage.getItem('filterModel'));
    this.filterRealEstate();
  }
  getRealEstateAddress(addressView: AddressView) {
    var street = addressView.AddressStreet.AddressStreetName;
    var city = addressView.AddressCity.AddressCityName;
    var region = addressView.AddressRegion.AddressRegionName;
    return `${city}, ${region}, ${street}, ${addressView.Address.HomeNumber}`;
  }
  
  public viewFullProduct(selectIndex: number) {
    this.selectRealEstate = this.realEstateList[selectIndex];
    this.isShowModal = true;
    this.istabImge = 0;
  }
  public closeModal() {
    this.isShowModal = !this.isShowModal;
    this.istabImge = 0;
  }
  public tabImge(indexImge) {
    this.istabImge = indexImge;
  }
  filterRealEstate() {
    this.realEstateService.filterRealEstateView(this.realEstateFilterView).subscribe(result => {
      this.realEstateList = result.Result;
    });
  }
  public selectRealEstateToOrder() {
    this.realEstateDataStoreService.setRealEstateForOrder(this.selectRealEstate);
    localStorage.setItem('realEstateForOrderGuid', this.selectRealEstate.RealEstate.RealEstateID.toString());
    this.router.navigate(["sign-up-to-view"]);
  }
}
