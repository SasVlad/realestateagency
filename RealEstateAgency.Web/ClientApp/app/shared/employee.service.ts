import { Injectable, Inject, Injector } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Http, URLSearchParams } from '@angular/http';
import { APP_BASE_HREF } from '@angular/common';
import { REQUEST } from '@nguniversal/aspnetcore-engine/tokens';
import { Employee } from '../models/employee-view';
import { Observable } from 'rxjs/Observable';


@Injectable()
export class EmployeeService {

  private baseUrl: string;

  constructor(
    private http: HttpClient,
    private injector: Injector
  ) {
    this.baseUrl = this.injector.get(REQUEST);
  }

  getEmployees() {
    return this.http.get<Employee[]>(`${this.baseUrl}/api/users`);
  }

  getEmployee(employeeGuid: string) {
    return this.http.get<any>(`${this.baseUrl}/Employee/GetEmployee?idEmployee=${employeeGuid}`);
  }

  getEmployeeView(employeeGuid: string) {
    return this.http.get<any>(`${this.baseUrl}/Employee/getEmployeeView?idEmployee=${employeeGuid}`);
  }

  deleteEmployee(employee: Employee) {
    return this.http.delete<Employee>(`${this.baseUrl}/api/users/` + employee.PersonId);
  }

  updateEmployee(employee: Employee){
    return this.http.post(`${this.baseUrl}/Employee/UpdateEmployee`, employee);
  }

  createEmployee(employee: Employee) {
    return this.http.post<any>(`${this.baseUrl}/Employee/CreateUser`, employee);
  }
}
