import { Injectable, Inject, Injector } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Http, URLSearchParams } from '@angular/http';
import { APP_BASE_HREF } from '@angular/common';
import { REQUEST } from '@nguniversal/aspnetcore-engine/tokens';
import { User } from '../models/user-view';
import { Observable } from 'rxjs/Observable';


@Injectable()
export class UserService {

  private baseUrl: string;

  constructor(
    private http: HttpClient,
    private injector: Injector
  ) {
    this.baseUrl = this.injector.get(REQUEST);
  }

  getUsers() {
    return this.http.get<User[]>(`${this.baseUrl}/api/users`);
  }

  getUser(userGuid: string) {
    return this.http.get<any>(`${this.baseUrl}/User/GetUser?userGuid=${userGuid}`);
  }
  getUserView(userGuid: string) {
    return this.http.get<any>(`${this.baseUrl}/User/GetUserView?userGuid=${userGuid}`);
  }

  deleteUser(user: User) {
    return this.http.delete<User>(`${this.baseUrl}/api/users/` + user.PersonId);
  }

  updateUser(user: User){
    return this.http.post(`${this.baseUrl}/User/UpdateUser`, user);
  }

  createUser(user: User) {
    return this.http.post<any>(`${this.baseUrl}/User/CreateUser`, user);
  }
}
