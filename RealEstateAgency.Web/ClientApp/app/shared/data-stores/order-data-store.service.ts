import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Employee, EmployeeView } from '../../models/employee-view';
import { OrderView } from '../../models/order-view';
import { OrderService } from '../order.service';
import { AddressView } from '../../models/address-view';
import { ResponseSignInModel } from '../../models/response-sign-in-model';


@Injectable()
export class OrderDataStoreService {

  private ordersListSource = new BehaviorSubject<OrderView[]>(null);
  ordersList = this.ordersListSource.asObservable();

  constructor(private orderService: OrderService) {
    //var userGuid = localStorage.getItem('personGuid');
    //let userRole = localStorage.getItem('personRole');
    //let personOrderModel = new ResponseSignInModel();
    //personOrderModel.Id = userGuid;
    //personOrderModel.Role = userRole;
    //this.orderService.getOrdersView(personOrderModel).subscribe(ordersList => {
    //  if (ordersList.Result != null) {
    //    this.setOrdersList(ordersList.Result);
    //  }
    //  //console.log("user not found");
    //})
  }


  setOrdersList(person: OrderView[]) {
    this.ordersListSource.next(person);
  }
}
