import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Employee } from '../../models/employee-view';
import { User } from '../../models/user-view';
import { AddressCity, AddressRegion, AddressStreet } from '../../models/address-view';
import { AddressService } from '../address.service';


@Injectable()
export class AddressDataStoreService {

  private citiesListSource = new BehaviorSubject<AddressCity[]>(null);
  citiesList = this.citiesListSource.asObservable();

  private regionsListSource = new BehaviorSubject<AddressRegion[]>(null);
  regionsList = this.regionsListSource.asObservable();

  private streetsListSource = new BehaviorSubject<AddressStreet[]>(null);
  streetsList = this.streetsListSource.asObservable();
  constructor(private addressService: AddressService) {
    console.log("addressDataStoreService call");
    this.addressService.getCities().subscribe((result) => {
      this.setCities(result.Result);
    });
    this.addressService.getRegions().subscribe((result) => {
      this.setRegions(result.Result);
    });
    this.addressService.getStreets().subscribe((result) => {
      this.setStreets(result.Result);
    });
  }

  setCities(cities: AddressCity[]) {
    this.citiesListSource.next(cities);
  }
  setRegions(regions: AddressRegion[]) {
    this.regionsListSource.next(regions);
  }
  setStreets(streets: AddressStreet[]) {
    this.streetsListSource.next(streets);
  }
}
