import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Employee, EmployeeView } from '../../models/employee-view';
import { User, UserView } from '../../models/user-view';
import { UserService } from '../user.service';
import { AddressView } from '../../models/address-view';


@Injectable()
export class UserDataStoreService {

  private userObjectSource = new BehaviorSubject<UserView|EmployeeView>(null);
  currentUser = this.userObjectSource.asObservable();

  constructor(private userService: UserService) {
    var userGuid = localStorage.getItem('personGuid');
    let userRole = localStorage.getItem('personRole');
    if (userGuid != null && userRole != null && userRole=="User") {
      this.userService.getUserView(userGuid).subscribe(user => {
        if (user.Result != null) {
          if (user.Result.AddressView == null) {
            user.Result.AddressView = new AddressView();
          }
          this.setUser(user.Result);
        }
        //console.log("user not found");
      })
    }
    console.log("userDataStoreService call");
  }

  setUser(person: UserView | EmployeeView) {
    this.userObjectSource.next(person);
  }
}
