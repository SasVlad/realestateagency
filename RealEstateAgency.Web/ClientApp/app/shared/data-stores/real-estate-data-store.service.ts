import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { RealEstateType, RealEstateTypeWall, RealEstateClass, RealEstate, RealEstateView } from '../../models/real-estate-view';
import { RealEstateService } from '../real-estate.service';


@Injectable()
export class RealEstateDataStoreService {

  private typesListSource = new BehaviorSubject<RealEstateType[]>(null);
  typesList = this.typesListSource.asObservable();

  private typeWallsListSource = new BehaviorSubject<RealEstateTypeWall[]>(null);
  typeWallsList = this.typeWallsListSource.asObservable();

  private classesListSource = new BehaviorSubject<RealEstateClass[]>(null);
  classesList = this.classesListSource.asObservable();

  private realEstateForOrderSource = new BehaviorSubject<RealEstateView>(null);
  realEstateForOrder = this.realEstateForOrderSource.asObservable();

  constructor(private realEstateService: RealEstateService) {
    console.log("realEstateDataStoreService call");
    let realEstateForOrderGuid = localStorage.getItem('realEstateForOrderGuid');
    if (realEstateForOrderGuid != null) {
      this.realEstateService.getRealEstateViewById(realEstateForOrderGuid).subscribe((result) => {
        this.setRealEstateForOrder(result.Result);
      })
    }
    this.realEstateService.getRealEstateTypes().subscribe((result) => {
      this.setTypes(result.Result);
    });
    this.realEstateService.getRealEstateTypeWalls().subscribe((result) => {
      this.setTypeWalls(result.Result);
    });
    this.realEstateService.getRealEstateClasses().subscribe((result) => {
      this.setClasses(result.Result);
    });
  }

  setTypes(types: RealEstateType[]) {
    this.typesListSource.next(types);
  }
  setTypeWalls(typeWalls: RealEstateTypeWall[]) {
    this.typeWallsListSource.next(typeWalls);
  }
  setClasses(classes: RealEstateClass[]) {
    this.classesListSource.next(classes);
  }
  setRealEstateForOrder(realEstate: RealEstateView) {
    this.realEstateForOrderSource.next(realEstate);
  }
}
