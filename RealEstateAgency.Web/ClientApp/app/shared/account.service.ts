import { Injectable, Inject, Injector } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Http, URLSearchParams } from '@angular/http';
import { APP_BASE_HREF } from '@angular/common';
import { REQUEST } from '@nguniversal/aspnetcore-engine/tokens';
import { Login } from '../models/login';
import { ResetPasswordModel } from '../models/reset-passwod-model';
import { ResponseSignInModel } from '../models/response-sign-in-model';
import { Observable } from 'rxjs/Observable';


@Injectable()
export class AccountService {

  private baseUrl: string;

  constructor(
    private http: HttpClient,
    private injector: Injector
  ) {
    this.baseUrl = this.injector.get(REQUEST);
  }

  Login(loginModel:Login) {
    return this.http.post<any>(`${this.baseUrl}/Account/LoginWeb`, loginModel)
  };
  resetPassword(resetPassword: ResetPasswordModel) {
    return this.http.post<any>(`${this.baseUrl}/Account/ResetPassword`, resetPassword)
  };
  //getStreets() {
  //  return this.http.get<any>(`${this.baseUrl}/Address/GetAllStreets`)
  //};
}
