import { Injectable, Inject, Injector } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Http, URLSearchParams } from '@angular/http';
import { APP_BASE_HREF } from '@angular/common';
import { REQUEST } from '@nguniversal/aspnetcore-engine/tokens';
import { UploadImage } from '../models/upload-image-model';
import { Observable } from 'rxjs/Observable';


@Injectable()
export class ImageService {

  private baseUrl: string;

  constructor(
    private http: HttpClient,
    private injector: Injector
  ) {
    this.baseUrl = this.injector.get(REQUEST);
  }
  uploadImage(image: UploadImage) {
    return this.http.post<any>(`${this.baseUrl}/Image/UploadImage`, image);
  }
}
