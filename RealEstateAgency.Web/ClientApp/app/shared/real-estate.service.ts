import { Injectable, Inject, Injector } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Http, URLSearchParams } from '@angular/http';
import { APP_BASE_HREF } from '@angular/common';
import { REQUEST } from '@nguniversal/aspnetcore-engine/tokens';
import { RealEstate,RealEstateView } from '../models/real-estate-view';
import { Observable } from 'rxjs/Observable';
import { RealEstateFilterView } from '../models/real-estate-filter-view';


@Injectable()
export class RealEstateService {

  private baseUrl: string;

  constructor(
    private http: HttpClient,
    private injector: Injector
  ) {
    this.baseUrl = this.injector.get(REQUEST);
  }

  getRealEstatesByEmployeeId(employeeId:string) {
    return this.http.get<any>(`${this.baseUrl}/RealEstate/GetAllRealEstatesViewByEmployeeId?employeeId=${employeeId}`);
  }
  getRealEstateViewById(idRealEstate: string) {
    return this.http.get<any>(`${this.baseUrl}/RealEstate/GetRealEstateViewById?idRealEstate=${idRealEstate}`);
  }

  getRealEstates() {
    return this.http.get<any>(`${this.baseUrl}/RealEstate/GetAllRealEstatesViewForCarousel`);
  }

  getRealEstateClasses() {
    return this.http.get<any>(`${this.baseUrl}/RealEstate/GetAllClasses`);
  }
  getRealEstateStatuses() {
    return this.http.get<any>(`${this.baseUrl}/RealEstate/GetAllStatuses`);
  }
  getRealEstateTypes() {
    return this.http.get<any>(`${this.baseUrl}/RealEstate/GetAllTypes`);
  }
  getRealEstateTypeWalls() {
    return this.http.get<any>(`${this.baseUrl}/RealEstate/GetAllTypeWalls`);
  } 
  filterRealEstateView(filterModel: RealEstateFilterView) {
    return this.http.post<any>(`${this.baseUrl}/RealEstate/FilterRealEstateView`, filterModel);
  }
  //deleteRealEstate(realEstate: RealEstate) {
  //  return this.http.delete<RealEstate>(`${this.baseUrl}/api/realEstates/` + realEstate.id);
  //}

  //updateRealEstate(realEstate: RealEstate) {
  //  return this.http.put<RealEstate>(`${this.baseUrl}/api/realEstates/` + realEstate.id, realEstate);
  //}

  addRealEstate(newRealEstateName: string) {
    return this.http.post<RealEstate>(`${this.baseUrl}/api/realEstates`, { name: newRealEstateName });
  }
}
