import { Injectable, Inject, Injector } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Http, URLSearchParams } from '@angular/http';
import { APP_BASE_HREF } from '@angular/common';
import { REQUEST } from '@nguniversal/aspnetcore-engine/tokens';
import { Order } from '../models/order';
import { Observable } from 'rxjs/Observable';
import { ResponseSignInModel } from '../models/response-sign-in-model';


@Injectable()
export class OrderService {

  private baseUrl: string;

  constructor(
    private http: HttpClient,
    private injector: Injector
  ) {
    this.baseUrl = this.injector.get(REQUEST);
  }

  getOrdersView(personOrderModel: ResponseSignInModel) {
    return this.http.post<any>(`${this.baseUrl}/Order/GetOrdersViewByPersonId`, personOrderModel);
  }

  getOrder(orderGuid: string) {
    return this.http.get<any>(`${this.baseUrl}/Order/GetOrder?idOrder=${orderGuid}`);
  }

  getOrderView(orderGuid: string) {
    return this.http.get<any>(`${this.baseUrl}/Order/getOrderView?idOrder=${orderGuid}`);
  }

  deleteOrder(order: Order) {
    return this.http.delete<Order>(`${this.baseUrl}/api/users/` + order.Id);
  }

  updateOrder(order: Order) {
    return this.http.post(`${this.baseUrl}/Order/UpdateOrder`, order);
  }

  createOrder(order: Order) {
    return this.http.post<any>(`${this.baseUrl}/Order/CreateOrder`, order);
  }

  getOrderBusyTimeByDate(date: string, realEstateId:number) {
    return this.http.get<any>(`${this.baseUrl}/Order/GetOrderBusyTimeByDate?date=${date}&realEstateId=${realEstateId}`);
  }
}
