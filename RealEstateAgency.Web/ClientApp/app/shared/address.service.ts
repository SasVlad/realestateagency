import { Injectable, Inject, Injector } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Http, URLSearchParams } from '@angular/http';
import { APP_BASE_HREF } from '@angular/common';
import { REQUEST } from '@nguniversal/aspnetcore-engine/tokens';
import { RealEstate, RealEstateView } from '../models/real-estate-view';
import { Observable } from 'rxjs/Observable';
import { Address } from '../models/address-view';


@Injectable()
export class AddressService {

  private baseUrl: string;

  constructor(
    private http: HttpClient,
    private injector: Injector
  ) {
    this.baseUrl = this.injector.get(REQUEST);
  }

  getCities() {
    return this.http.get<any>(`${this.baseUrl}/Address/GetAllCities`)
  };
  getRegions() {
    return this.http.get<any>(`${this.baseUrl}/Address/AllRegions`)
  };
  getStreets() {
    return this.http.get<any>(`${this.baseUrl}/Address/GetAllStreets`)
  };
  createAddress(address: Address) {
    return this.http.post<any>(`${this.baseUrl}/Address/CreateAddress`, address);
  }
  updateAddress(address: Address) {
    return this.http.post<any>(`${this.baseUrl}/Address/UpdateAddress`, address);
  }
}
