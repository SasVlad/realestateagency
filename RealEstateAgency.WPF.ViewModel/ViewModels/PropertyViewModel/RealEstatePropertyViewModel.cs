﻿using RealEstateAgency.WPF.Model.Models.ModelDTO;
using RealEstateAgency.WPF.Model.Models.ModelViewDTO;
using RealEstateAgency.WPF.Model.Services;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace RealEstateAgency.WPF.ViewModel.ViewModels.PropertyViewModel
{
    public class RealEstatePropertyViewModel : INotifyPropertyChanged
    {
        private ObservableCollection<RealEstateClassDTO> realEstateClassesList = new ObservableCollection<RealEstateClassDTO>();
        private ObservableCollection<RealEstateStatusDTO> realEstateStatusesList = new ObservableCollection<RealEstateStatusDTO>();
        private ObservableCollection<RealEstateTypeDTO> realEstateTypesList = new ObservableCollection<RealEstateTypeDTO>();
        private ObservableCollection<RealEstateTypeWallDTO> realEstateTypeWallsList = new ObservableCollection<RealEstateTypeWallDTO>();
        private RealEstateViewDTO RealEstateModel;
        private AddressPropertyViewModel AddressModel;
        public event PropertyChangedEventHandler PropertyChanged;

        public RealEstatePropertyViewModel(RealEstateViewDTO realEstateModel)
        {
            AddressViewModel = AddressViewModel ?? new AddressPropertyViewModel(realEstateModel.AddressView);
            RealEstateModel = realEstateModel ?? new RealEstateViewDTO();

            InsertTextBoxRealEstateInformation(RealEstateModel);
            AddressViewModel.InsertComboboxAddressInformation(realEstateModel.AddressView);
            ThreadPool.QueueUserWorkItem(InokeAsyncMethods);
        }
        public RealEstateViewDTO GetRealEstate {
            get {
                //RealEstateModel.RealEstate.RealEstateStatusID = 1;
                return RealEstateModel;
            }
        }
        public AddressPropertyViewModel AddressViewModel
        {
            get { return AddressModel; }
            set
            {
                AddressModel = value;
                OnPropertyChanged("AddressViewModel");
            }
        }
        private async void InokeAsyncMethods(Object state)
        {
            cbClassesSource = ToObservableCollection<RealEstateClassDTO>(await new RealEstateClassService().GetAllRealEstateClasses());
            cbStatusesSource = ToObservableCollection<RealEstateStatusDTO>(await new RealEstateStatusService().GetAllRealEstateStatuses());
            cbTypesSource = ToObservableCollection<RealEstateTypeDTO>(await new RealEstateTypeService().GetAllRealEstateTypes());
            cbTypeWallsSource = ToObservableCollection<RealEstateTypeWallDTO>(await new RealEstateTypeWallService().GetAllRealEstateTypeWalls());                      
        }

        public void InsertTextBoxRealEstateInformation(RealEstateViewDTO realEstate)
        {           
            cbTypeIdSelected = realEstate.RealEstate.RealEstateTypeID;
            cbStatusIdSelected = realEstate.RealEstate.RealEstateStatusID;
            cbTypeWallIdSelected = realEstate.RealEstate.RealEstateTypeWallID;
            cbClassIdSelected = realEstate.RealEstate.RealEstateClassID;
            TbPrice = realEstate.RealEstate.Price;
            TbNumberOfRooms = realEstate.RealEstate.NumberOfRooms;
            TbLevel = realEstate.RealEstate.Level;
            TbGrossArea = realEstate.RealEstate.GrossArea;
            TbNearSubway = realEstate.RealEstate.NearSubway;
            CheckElevator = realEstate.RealEstate.Elevator;
            RealEstateModel.RealEstate.AddressID = realEstate.RealEstate.AddressID;
            RealEstateModel.RealEstate.RealEstateID = realEstate.RealEstate.RealEstateID;
            RealEstateModel.RealEstate.EmployeeId = realEstate.RealEstate.EmployeeId;
            RealEstateModel.RealEstate.ImageUrls = realEstate.RealEstate.ImageUrls;
            GetImageUrls();
        }
        
        private ObservableCollection<T> ToObservableCollection<T>(List<T> list)
        {
            var observList = new ObservableCollection<T>();
            foreach (var item in list)
                observList.Add(item);
            return observList;
        }
        
        public bool CheckElevator
        {
            get { return RealEstateModel.RealEstate.Elevator; }
            set
            {
                RealEstateModel.RealEstate.Elevator = value;
                OnPropertyChanged("CheckElevator");
            }
        }
        public double TbPrice
        {
            get { return RealEstateModel.RealEstate.Price; }
            set
            {
                RealEstateModel.RealEstate.Price = value;
                OnPropertyChanged("TbPrice");
            }
        }
        public int TbNumberOfRooms
        {
            get { return RealEstateModel.RealEstate.NumberOfRooms; }
            set
            {
                RealEstateModel.RealEstate.NumberOfRooms = value;
                OnPropertyChanged("TbNumberOfRooms");
            }
        }
        public int TbLevel
        {
            get { return RealEstateModel.RealEstate.Level; }
            set
            {
                RealEstateModel.RealEstate.Level = value;
                OnPropertyChanged("TbLevel");
            }
        }
        public double TbGrossArea
        {
            get { return RealEstateModel.RealEstate.GrossArea; }
            set
            {
                RealEstateModel.RealEstate.GrossArea = value;
                OnPropertyChanged("TbGrossArea");
            }
        }
        public string TbNearSubway
        {
            get { return RealEstateModel.RealEstate.NearSubway; }
            set
            {
                RealEstateModel.RealEstate.NearSubway = value;
                OnPropertyChanged("TbNearSubway");
            }
        }

        public ObservableCollection<RealEstateTypeDTO> cbTypesSource
        {
            get { return realEstateTypesList; }
            set
            {
                realEstateTypesList = value;
                OnPropertyChanged("cbTypesSource");
            }
        }

        public int cbTypeIdSelected
        {
            get { return RealEstateModel.RealEstate.RealEstateTypeID; }
            set
            {
                RealEstateModel.RealEstate.RealEstateTypeID = value;
                OnPropertyChanged("cbTypeIdSelected");
            }
        }

        public ObservableCollection<RealEstateStatusDTO> cbStatusesSource
        {
            get { return realEstateStatusesList; }
            set
            {
                realEstateStatusesList = value;
                OnPropertyChanged("cbStatusesSource");
            }
        }

        public int cbStatusIdSelected
        {
            get { return RealEstateModel.RealEstate.RealEstateStatusID; }
            set
            {
                RealEstateModel.RealEstate.RealEstateStatusID = value;
                OnPropertyChanged("cbStatusIdSelected");
            }
        }

        public ObservableCollection<RealEstateTypeWallDTO> cbTypeWallsSource
        {
            get { return realEstateTypeWallsList; }
            set
            {
                realEstateTypeWallsList = value;
                OnPropertyChanged("cbTypeWallsSource");
            }
        }

        public int cbTypeWallIdSelected
        {
            get { return RealEstateModel.RealEstate.RealEstateTypeWallID; }
            set
            {
                RealEstateModel.RealEstate.RealEstateTypeWallID = value;
                OnPropertyChanged("cbTypeWallIdSelected");
            }
        }

        public ObservableCollection<RealEstateClassDTO> cbClassesSource
        {
            get { return realEstateClassesList; }
            set
            {
                realEstateClassesList = value;
                OnPropertyChanged("cbClassesSource");
            }
        }

        public int cbClassIdSelected
        {
            get { return RealEstateModel.RealEstate.RealEstateClassID; }
            set
            {
                RealEstateModel.RealEstate.RealEstateClassID = value;
                OnPropertyChanged("cbClassIdSelected");
            }
        }        

        protected void OnPropertyChanged(string PropertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(PropertyName));
            }
        }
        public void GetImageUrls()
        {
            var images = string.IsNullOrEmpty(RealEstateModel.RealEstate.ImageUrls) ? new List<string>() : RealEstateModel.RealEstate.ImageUrls.Split('|').ToList();
            if (images.Count >= 1)
                 { ImageUrl_0 = images[0]; }
                 else { ImageUrl_0 = ""; }
            if (images.Count >= 1)
                { ImageUrl_1 = images[0]; }
                else { ImageUrl_1 = ""; }
            if (images.Count >= 2)
                { ImageUrl_2 = images[1]; }
                else { ImageUrl_2 = ""; }
            if (images.Count >= 3)
                { ImageUrl_3 = images[2]; }
                else { ImageUrl_3 = ""; }
            if (images.Count >= 4)
                { ImageUrl_4 = images[3]; }
                else { ImageUrl_4 = ""; }
        }
        public void SetImageUrls(string imageUrl)
        {
            var images = string.IsNullOrEmpty(RealEstateModel.RealEstate.ImageUrls)?new List<string>(): RealEstateModel.RealEstate.ImageUrls.Split('|').ToList();
            images.Add(imageUrl);
            RealEstateModel.RealEstate.ImageUrls = string.Join("|", images);
        }
        private string _imageUrl_0;
        public string ImageUrl_0
        {
            get { return string.IsNullOrEmpty(_imageUrl_0) ? "Images/no-image.gif" : "http://localhost:51608/images/" + _imageUrl_0; }
            set
            {
                _imageUrl_0 = value;
                OnPropertyChanged("ImageUrl_0");
            }
        }
        private string _imageUrl_1;
        public string ImageUrl_1
        {
            get { return string.IsNullOrEmpty(_imageUrl_1) ? "Images/no-image.gif" : "http://localhost:51608/images/" + _imageUrl_1; }
            set
            {
                _imageUrl_1 = value;
                OnPropertyChanged("ImageUrl_1");
            }
        }
        private string _imageUrl_2;
        public string ImageUrl_2
        {
            get { return string.IsNullOrEmpty(_imageUrl_2) ? "Images/no-image.gif" : "http://localhost:51608/images/" + _imageUrl_2; }
            set
            {
                _imageUrl_2 = value;
                OnPropertyChanged("ImageUrl_2");
            }
        }
        private string _imageUrl_3;
        public string ImageUrl_3
        {
            get { return string.IsNullOrEmpty(_imageUrl_3) ? "Images/no-image.gif" : "http://localhost:51608/images/" + _imageUrl_3; }
            set
            {
                _imageUrl_3 = value;
                OnPropertyChanged("ImageUrl_3");
            }
        }
        private string _imageUrl_4;
        public string ImageUrl_4
        {
            get { return string.IsNullOrEmpty(_imageUrl_4) ? "Images/no-image.gif" : "http://localhost:51608/images/" + _imageUrl_4; }
            set
            {
                _imageUrl_4= value;
                OnPropertyChanged("ImageUrl_4");
            }
        }
    }
}
