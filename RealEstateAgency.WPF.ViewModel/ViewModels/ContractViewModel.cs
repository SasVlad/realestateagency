﻿using RealEstateAgency.WPF.Model.Models.ModelDTO;
using RealEstateAgency.WPF.Model.Models.ModelViewDTO;
using RealEstateAgency.WPF.Model.Services;
using RealEstateAgency.WPF.ViewModel.Helpers;
using RealEstateAgency.WPF.ViewModel.Infrastructure;
using RealEstateAgency.WPF.ViewModel.Model;
using RealEstateAgency.WPF.ViewModel.ViewModels.PropertyViewModel;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Input;

namespace RealEstateAgency.WPF.ViewModel.ViewModels
{
    public class ContractViewModel : INotifyPropertyChanged
    {
        private PersonPropertyViewModel<UserDTO> PersonModel; 
        private PersonPropertyViewModel<EmployeeDTO> PersonEmployeeModel;
        private RealEstatePropertyViewModel RealEstatePropertyModel;
        private MiddleClassModel middleModel = null;
        private string _tbPost = "";
        public event PropertyChangedEventHandler PropertyChanged;
        private ContractDTO Contract = new ContractDTO();
        public MiddleClassModel MiddleModel
        {
            get { return middleModel; }
            set
            {
                middleModel = value;

                PersonViewModel = new PersonPropertyViewModel<UserDTO>(MiddleModel.User ?? new UserViewDTO() {Person = new UserDTO(), AddressView =new AddressViewDTO() });               
                RealEstatePropertyModel = new RealEstatePropertyViewModel(MiddleModel.RealEstate ?? new RealEstateViewDTO { RealEstate = new RealEstateDTO(), AddressView = new AddressViewDTO() });
                //RealEstatePropertyViewModel.InsertTextBoxRealEstateInformation(MiddleModel.RealEstate ?? new RealEstateViewDTO { RealEstate = new RealEstateDTO(), Address = new AddressDTO() });
                if (middleModel.Employee != null)
                {
                    PersonViewEmployeeModel = new PersonPropertyViewModel<EmployeeDTO>(MiddleModel.Employee??new EmployeeViewDTO {Person=new EmployeeDTO(),AddressView=new AddressViewDTO(),Dismisses=new List<EmployeeDismissDTO>() });
                    ThreadPool.QueueUserWorkItem(InokeAsyncMethods);
                }                                
            }
        }
        private DelegateCommand saveContract;
        public ICommand SaveContract
        {
            get { return saveContract; }
        }
        public event Action ShowMainScreenAction;
        private DelegateCommand showMainWindow;
        public ICommand ShowMainWindow
        {
            get { return showMainWindow; }
        }
        public event Action ShowRealEstateScreenAction;
        private DelegateCommand showRealEstateWindow;
        public ICommand ShowRealEstateWindow
        {
            get { return showRealEstateWindow; }
        }
        public event Action ShowUserScreenAction;
        private DelegateCommand showUserWindow;
        public ICommand ShowUserWindow
        {
            get { return showUserWindow; }
        }
        private DelegateCommand _ShowImageDialogMenu;
        public ICommand BtnShowImageDialogMenu
        {
            get { return _ShowImageDialogMenu; }
        }
        private DelegateCommand _clearRealEstateFields;
        public ICommand ClearRealEstateFieldsCommand
        {
            get { return _clearRealEstateFields; }
        }
        private DelegateCommand _clearClientFields;
        public ICommand ClearClientFieldsCommand
        {
            get { return _clearClientFields; }
        }      
        private FuncCommand _deleteImage;
        public ICommand BtnDeleteImageCommand
        {
            get { return _deleteImage; }
        }
        private FuncCommand _changeImage;
        public ICommand BtnChangeImageCommand
        {
            get { return _changeImage; }
        }
        public string TbPost
        {
            get { return _tbPost; }
            set
            {
                _tbPost = value;
                OnPropertyChanged("TbPost");
            }
        }
        public string TbDateContract
        {
            get { return Contract.RecordDate; }
            set
            {
                Contract.RecordDate = value;
                OnPropertyChanged("TbDateContract");
            }
        }
        string _tbTypeContract;
        public string TbTypeContract
        {
            get { return _tbTypeContract; }
            set
            {
                _tbTypeContract = value;
                OnPropertyChanged("TbTypeContract");
            }
        }
        bool _viewModeRealEstate;
        public bool ViewModeRealEstate
        {
            get { return _viewModeRealEstate; }
            set
            {
                _viewModeRealEstate = value;
                OnPropertyChanged("ViewModeRealEstate");
            }
        }
        bool _viewModeUser;
        public bool ViewModeUser
        {
            get { return _viewModeUser; }
            set
            {
                _viewModeUser = value;
                OnPropertyChanged("ViewModeUser");
            }
        }
        private void ShowMainScreen()
        {
            if (ShowMainScreenAction != null)
            {
                ShowMainScreenAction.Invoke();
            }
        }
        private void ShowRealEstateScreen()
        {
            if (ShowRealEstateScreenAction != null)
            {
                ShowRealEstateScreenAction.Invoke();
            }
        }
        private void ShowUserScreen()
        {
            if (ShowUserScreenAction != null)
            {
                ShowUserScreenAction.Invoke();
            }
        }
        public PersonPropertyViewModel<UserDTO> PersonViewModel {
            get { return PersonModel; }
            set {
                PersonModel = value;
                OnPropertyChanged("PersonViewModel");
            }
        }
        public PersonPropertyViewModel<EmployeeDTO> PersonViewEmployeeModel
        {
            get { return PersonEmployeeModel; }
            set
            {
                PersonEmployeeModel = value;
                OnPropertyChanged("PersonEmployeeModel");
            }
        }
        public RealEstatePropertyViewModel RealEstatePropertyViewModel
        {
            get { return RealEstatePropertyModel; }
            set
            {
                RealEstatePropertyModel = value;
                OnPropertyChanged("RealEstatePropertyViewModel");
            }
        }
        public ContractViewModel()
        {
            showMainWindow = new DelegateCommand(ShowMainScreen);
            showRealEstateWindow = new DelegateCommand(ShowRealEstateScreen);
            showUserWindow = new DelegateCommand(ShowUserScreen);
            saveContract = new DelegateCommand(SaveContractMethod);
            _ShowImageDialogMenu = new DelegateCommand(ShowImageDialogMenu);
            _deleteImage = new FuncCommand(DeleteImage);
            _changeImage = new FuncCommand(ChangeImage);
            _clearRealEstateFields = new DelegateCommand(ClearRealEstateFields);
            _clearClientFields = new DelegateCommand(ClearClientFields);
        }

        private async void SaveContractMethod()
        {
            Contract.EmployeeID = MiddleModel.Employee.Person.PersonId;
            var newClient = new UserDTO();
            if (MiddleModel.User == null)
            {
                newClient = (UserDTO)PersonViewModel.GetPerson;
                newClient.Role = "User";
            }
            Contract.SellerID = MiddleModel.User!=null?
                                    MiddleModel.User.Person.PersonId:
                                    await new UserService()
                                            .CreateUser(new UserViewDTO()
                                                        {
                                                        Person = newClient,
                                                        AddressView=PersonViewModel.AddressViewModel.GetAddressModel
                                                        });
            var newRealEstate = new RealEstateDTO();
            if (MiddleModel.RealEstate == null)
            {
                newRealEstate = RealEstatePropertyViewModel.GetRealEstate.RealEstate;
                newRealEstate.EmployeeId = Contract.EmployeeID;
                newRealEstate.RealEstateStatusID = 2;
            }

            Contract.RealEstateID = MiddleModel.RealEstate != null?
                                            MiddleModel.RealEstate.RealEstate.RealEstateID:
                                            await new RealEstateService()
                                                    .CreateRealEstate(new RealEstateViewDTO()
                                                          {
                                                          RealEstate = newRealEstate,//RealEstatePropertyViewModel.GetRealEstate.RealEstate,
                                                          AddressView = RealEstatePropertyViewModel.AddressViewModel.GetAddressModel
                                                          });

            await new ContractService().CreateContract(Contract);
        }
        private void ClearRealEstateFields()
        {
            MiddleModel.RealEstate = null;
            RealEstatePropertyViewModel = new RealEstatePropertyViewModel(new RealEstateViewDTO());
            RealEstatePropertyViewModel.cbTypeIdSelected = 0;
            RealEstatePropertyViewModel.cbTypeWallIdSelected = 0;
            RealEstatePropertyViewModel.TbPrice = 0;
            RealEstatePropertyViewModel.TbNumberOfRooms = 0;
            RealEstatePropertyViewModel.TbLevel = 0;
            RealEstatePropertyViewModel.TbGrossArea = 0;
            RealEstatePropertyViewModel.TbNearSubway = null;
            RealEstatePropertyViewModel.CheckElevator = false;
            RealEstatePropertyViewModel.AddressViewModel.SelectedCityId = 0;
            RealEstatePropertyViewModel.AddressViewModel.SelectedRegionId = 0;
            RealEstatePropertyViewModel.AddressViewModel.SelectedStreetId = 0;
            RealEstatePropertyViewModel.AddressViewModel.TbHomeNumber = null;
            RealEstatePropertyViewModel.AddressViewModel.TbApartmentNumber = 0;
            RealEstatePropertyViewModel.GetImageUrls();
        }
        private void ClearClientFields()
        {
            MiddleModel.User = null;
            PersonViewModel = new PersonPropertyViewModel<UserDTO>(new UserViewDTO());
            PersonViewModel.TbSurname = null;
            PersonViewModel.TbName = null;
            PersonViewModel.TbPatronumic = null;
            PersonViewModel.TbPhoneNumber = 0;
            PersonViewModel.TbPassport = null;
            PersonViewModel.TbEmail = null;
            PersonViewModel.TbPassword = null;
            PersonViewModel.AddressViewModel.SelectedCityId = 0;
            PersonViewModel.AddressViewModel.SelectedRegionId = 0;
            PersonViewModel.AddressViewModel.SelectedStreetId = 0;
            PersonViewModel.AddressViewModel.TbHomeNumber = null;
            PersonViewModel.AddressViewModel.TbApartmentNumber = 0;

        }
        private async void InokeAsyncMethods(Object stateInfo)
        {
            
            ViewModeUser = MiddleModel.User != null ? false : true;
            ViewModeRealEstate = MiddleModel.RealEstate != null ? false : true;
            TbDateContract=DateTime.Now.ToShortDateString();
            ContractTypeDTO contractType = (await new ContractTypeService().GetAllContractTypes()).Where(c => c.ContractTypeID == (MiddleModel.RealEstate != null ? 1 : 2)).FirstOrDefault();
            TbTypeContract = contractType.ContractTypeName;
            Contract.ContractTypeID = contractType.ContractTypeID;
            TbPost = (await new EmployeePostService().GetAllPosts())
                .Where(ep => ep.EmployeePostID == MiddleModel.Employee.Person.EmployeePostID)
                .FirstOrDefault()
                .EmployeePostName;
        }
        public ContractViewModel(MiddleClassModel middleModel)
        {
            MiddleModel = middleModel;           
        }
        protected void OnPropertyChanged(string PropertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(PropertyName));
            }
        }
        private async void ShowImageDialogMenu()
        {
            var uploadImageModel = new ImageHelper().GetImageBase64FromDialogMenu();
            var resultUploadImage = await new ImageService().UploadImage(uploadImageModel);
            if (resultUploadImage.Result.Successed)
            {
                RealEstatePropertyViewModel.SetImageUrls(resultUploadImage.Result.Message);
                RealEstatePropertyViewModel.GetImageUrls();
            }
        }
        private async void DeleteImage(object image)
        {
            var imageName = ((string)image).Split('/').Last();
            var images = string.IsNullOrEmpty(RealEstatePropertyViewModel.GetRealEstate.RealEstate.ImageUrls) ? new List<string>() : RealEstatePropertyViewModel.GetRealEstate.RealEstate.ImageUrls.Split('|').ToList();
            images.Remove(imageName);
            RealEstatePropertyViewModel.GetRealEstate.RealEstate.ImageUrls = string.Join("|", images);
            RealEstatePropertyViewModel.GetImageUrls();
        }
        private async void ChangeImage(object image)
        {
            var imageName = ((string)image).Split('/').Last();
            RealEstatePropertyViewModel.ImageUrl_0 = imageName;
        }
    }
}
