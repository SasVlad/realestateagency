﻿using Microsoft.Win32;
using RealEstateAgency.WPF.Model.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace RealEstateAgency.WPF.ViewModel.Helpers
{
    public class ImageHelper
    {
        public UploadImage GetImageBase64FromDialogMenu()
        {
            var uploadImageModel = new UploadImage();
            Stream myStream = null;
            OpenFileDialog dialog = new OpenFileDialog();
            dialog.Filter = "Files(*.BMP;*.JPG;*.GIF)|*.BMP;*.JPG;*.GIF|All files (*.*)|*.*";
            Nullable<bool> result = dialog.ShowDialog();
            if (result == true)
            {
                try
                {
                    if ((myStream = dialog.OpenFile()) != null)
                    {
                        using (myStream)
                        {
                            string sFileName = dialog.FileName;
                            
                            Byte[] bytes = File.ReadAllBytes($"{dialog.FileName}");
                            String file = Convert.ToBase64String(bytes);
                            uploadImageModel = new UploadImage()
                            {
                                ImageBase64 = file,
                                ImageName = dialog.SafeFileName
                            };                            
                        }
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Something went wrong: " + ex.Message);
                }
            }
            return uploadImageModel;
        }
    }
}
