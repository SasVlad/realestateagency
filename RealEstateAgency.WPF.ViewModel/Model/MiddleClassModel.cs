﻿using RealEstateAgency.WPF.Model.Models;
using RealEstateAgency.WPF.Model.Models.ModelDTO;
using RealEstateAgency.WPF.Model.Models.ModelViewDTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RealEstateAgency.WPF.ViewModel.Model
{
    public class MiddleClassModel
    {
        public EmployeeViewDTO Employee { get; set; }
        public UserViewDTO User { get; set; }
        public RealEstateViewDTO RealEstate { get; set; }
    }
}
